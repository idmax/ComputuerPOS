<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>主页</title>
</head>

<body>
	<div>
	<h1>权限测试</h1>
		<button id="addPer">addPer</button>
		<button id="deletePer">deletePer</button>
		<button id="getPer">getPer</button>
		<button id="getAllPer">getAllPer</button>
		<button id="getRole">getRole</button>
		<button id="addRole">addRole</button>
		<button id="deleteRole">deleteRole</button>
		<button id="getAllRole">getAllRole</button>
		<button id="getRolePer">getRolePer</button>
		<button id="addRolePer">addRolePer</button>
		<button id="deleteRolePer">deleteRolePer</button>
		<button id="getUserRole">getUserRole</button>
		<button id="addUserRole">addUserRole</button>
		<button id="deleteUserRole">deleteUserRole</button>
		<button id="updatePermission">updatePermission</button>
		<button id="updateRole">updateRole</button>
	</div>
	<hr>
	<div>
		<h1>用户测试</h1>
		<button id="addUser">addUser</button>
		<button id="updateUser">updateUser</button>
		<button id="searchUser">searchUser</button>
		<button id="getUserByName">getUserByName</button>
	</div>
	<hr>
	<div>
		<h1>退货测试</h1>
		<button id="createReturn">createReturn</button>
		<button id="addReturnItem">addReturnItem</button>
	</div>
	<div>
		<h1>商品测试</h1>
		<button id="searchGoods">searchGoods</button>
		<button id="updateGoods">updateGoods</button>
		<button id="updateType">updateType</button>
		<button id="addType">addType</button>
		<button id="deleteType">deleteType</button>
		<button id="getTypeById">getType</button>
		<button id="getTypeByTypeName">getTypeByTypeName</button>
		<button id="getGoodsByGoodsName">getGoodsByGoodsName</button>
		<button id="getGoodsByMultiCondition">getGoodsByMultiCondition</button>
	</div>
	<div>
		<h1>Order测试</h1>
		<button id="getAllOrder">getAllOrder</button>
		<button id="getAllOrderItem">getAllOrderItem</button>
		<button id="getOrderItemByOrderId">getOrderItemByOrderId</button>
		<button id="getOrderByTime">getOrderByTime</button>
		<button id="getOrderByUserName">getOrderByUserName</button>
		<button id="getOrderItemByTime">getOrderItemByTime</button>
	</div>
</body>
<script src="js/jquery.min.js"></script>
<script>

var addPer={
		"per.permissionName":"name"		
}
,deletePer={
		"per.id":1		
}
,getPer={
		"per.id":2
}
,addUser={
		"user.UserNumber":"ing",
		"user.UserName":"yajin",
		"user.Gender":"g",
		"userRole.roleId":2
},
getRole={
		"role.roleId":3
}
,addRole={
		"role.roleName":"name"
},
deleteRole={
		"role.roleId":1
},
createReturn={
		"saleReturn.salereturnNo":"1111",
		"saleReturn.order.orderId":2,
		"saleReturn.reason":"reson",
		"saleReturn.total":12.2
},
addReturnItem={
		"saleReturnItem.salereturn.id":2,
		"saleReturnItem.goods.GoodsId":2,
		"saleReturnItem.quantity":1,
		"saleReturnItem.price":12.2,
		"orderItem.orderItemId":2
},
getRolePer={
		"rolePer.roleId":2
}
,addRolePer={
		"rolePer.roleId":2,
		"rolePer.permissionId":1
},
deleteRolePer={
		"rolePer.id":1
},
addUserRole={
		"userRole.userId":1,
		"userRole.roleId":2
}
,deleteUserRole={
		"userRole.id":1
}
,getUserRole={
		"userRole.userId":1
}
,searchGoods={
		"goods.goodsNumber":1003
},
getOrderItemByOrderId={
		"order.orderId":2
},
updateGoods={
		"goods.price":11,
		"goods.discount":0.5,
		"goods.goodsId":1,
		"goods.type.typeId":2
},
updateType={
		"type.typeName":"update",
		"type.typeId":2
},
addType={
		"type.typeName":"newType2",
		"type.desription":"description2"
},
deleteType={
		"type.typeId":4
},
getTypeById={
		"type.typeId":2
},
updateUser={
		"user.userId":3,
		"user.userName":"newNamez"
},
updatePermission={
		"per.id":1,
		"per.permissionName":"new3"
},
updateRole={
		"role.roleId":2,
		"role.roleName":"new"
},
searchUser={
		"user.userNumber":"00"
},
getOrderByTime={
		"start":"2015-04-21 01:11:41",
		"end":"2015-05-23 22:12:26"
},
getGoodsByGoodsName={
		"goods.goodsName":"nu"
}
getGoodsByMultiCondition={
		"goods.goodsName":"um",
		"goods.goodsNumber":1
},
getUserByName={
		"user.userName":"n"
},
getTypeByTypeName={
		"type.typeName":"e"
},
getOrderByUserName={
		"userName":"uu"
},
getOrderItemByTime={
		"start":"2015-01-26 22:35:15",
		"end":"2015-07-26 22:35:50"
}
$("#getOrderItemByTime").click(function(){
	 $.ajax({
	url:"Order/getOrderItemByTime.action",
	data:getOrderItemByTime,
	type:"get",
	dataType:"json",
	success:function(data,textStatus,jq){
/*data、就是ajax的返回值、一个json对象*/

},
	error:function(data,textStatus,jq){}
})
});
$("#getOrderByUserName").click(function(){
	 $.ajax({
	url:"Order/getOrderByUserName.action",
	data:getOrderByUserName,
	type:"get",
	dataType:"json",
	success:function(data,textStatus,jq){
/*data、就是ajax的返回值、一个json对象*/

},
	error:function(data,textStatus,jq){}
})
});
$("#getTypeByTypeName").click(function(){
	 $.ajax({
	url:"Goods/getTypeByTypeName.action",
	data:getTypeByTypeName,
	type:"get",
	dataType:"json",
	success:function(data,textStatus,jq){
/*data、就是ajax的返回值、一个json对象*/

},
	error:function(data,textStatus,jq){}
})
});
$("#getUserByName").click(function(){
	 $.ajax({
	url:"User/getUserByName.action",
	data:getUserByName,
	type:"get",
	dataType:"json",
	success:function(data,textStatus,jq){
/*data、就是ajax的返回值、一个json对象*/

},
	error:function(data,textStatus,jq){}
})
});
$("#getGoodsByMultiCondition").click(function(){
	 $.ajax({
	url:"Goods/getGoodsByMultiCondition.action",
	data:getGoodsByMultiCondition,
	type:"get",
	dataType:"json",
	success:function(data,textStatus,jq){
/*data、就是ajax的返回值、一个json对象*/

},
	error:function(data,textStatus,jq){}
})
});
$("#getGoodsByGoodsName").click(function(){
	 $.ajax({
	url:"Goods/getGoodsByGoodsName.action",
	data:getGoodsByGoodsName,
	type:"get",
	dataType:"json",
	success:function(data,textStatus,jq){
/*data、就是ajax的返回值、一个json对象*/

},
	error:function(data,textStatus,jq){}
})
});
$("#getOrderByTime").click(function(){
	 $.ajax({
	url:"Order/getOrderByTime.action",
	data:getOrderByTime,
	type:"get",
	dataType:"json",
	success:function(data,textStatus,jq){
 /*data、就是ajax的返回值、一个json对象*/

},
	error:function(data,textStatus,jq){}
})
});
$("#searchUser").click(function(){
	 $.ajax({
	url:"User/searchUser.action",
	data:searchUser,
	type:"get",
	dataType:"json",
	success:function(data,textStatus,jq){
  /*data、就是ajax的返回值、一个json对象*/
 
},
	error:function(data,textStatus,jq){}
})
});
$("#updatePermission").click(function(){
	 $.ajax({
	url:"Permission/updatePermission.action",
	data:updatePermission,
	type:"get",
	dataType:"json",
	success:function(data,textStatus,jq){
   /*data、就是ajax的返回值、一个json对象*/
  
},
	error:function(data,textStatus,jq){}
})
});
$("#updateRole").click(function(){
	 $.ajax({
	url:"Permission/updateRole.action",
	data:updateRole,
	type:"get",
	dataType:"json",
	success:function(data,textStatus,jq){
   /*data、就是ajax的返回值、一个json对象*/
  
},
	error:function(data,textStatus,jq){}
})
});
$("#updateUser").click(function(){
	 $.ajax({
	url:"User/updateUser.action",
	data:updateUser,
	type:"get",
	dataType:"json",
	success:function(data,textStatus,jq){
    /*data、就是ajax的返回值、一个json对象*/
   
},
	error:function(data,textStatus,jq){}
})
});
$("#updateGoods").click(function(){
	 $.ajax({
	url:"Goods/updateGoods.action",
	data:updateGoods,
	type:"get",
	dataType:"json",
	success:function(data,textStatus,jq){
     /*data、就是ajax的返回值、一个json对象*/
    
 },
	error:function(data,textStatus,jq){}
})
});
$("#updateType").click(function(){
	 $.ajax({
	url:"Goods/updateType.action",
	data:updateType,
	type:"get",
	dataType:"json",
	success:function(data,textStatus,jq){
      /*data、就是ajax的返回值、一个json对象*/
     
  },
	error:function(data,textStatus,jq){}
})
});
$("#getTypeById").click(function(){
	 $.ajax({
	url:"Goods/getTypeById.action",
	data:getTypeById,
	type:"get",
	dataType:"json",
	success:function(data,textStatus,jq){
     /*data、就是ajax的返回值、一个json对象*/
    
 },
	error:function(data,textStatus,jq){}
})
});
$("#addType").click(function(){
	 $.ajax({
	url:"Goods/addType.action",
	data:addType,
	type:"get",
	dataType:"json",
	success:function(data,textStatus,jq){
     /*data、就是ajax的返回值、一个json对象*/
    
 },
	error:function(data,textStatus,jq){}
})
});
$("#deleteType").click(function(){
	 $.ajax({
	url:"Goods/deleteType.action",
	data:deleteType,
	type:"get",
	dataType:"json",
	success:function(data,textStatus,jq){
     /*data、就是ajax的返回值、一个json对象*/
    
 },
	error:function(data,textStatus,jq){}
})
});
$("#getOrderItemByOrderId").click(function(){
	 $.ajax({
	url:"Order/getOrderItemByOrderId.action",
	data:getOrderItemByOrderId,
	type:"get",
	dataType:"json",
	success:function(data,textStatus,jq){
       /*data、就是ajax的返回值、一个json对象*/
      
   },
	error:function(data,textStatus,jq){}
})
});
$("#getAllOrderItem").click(function(){
	 $.ajax({
	url:"Order/getAllOrderItem.action",
	
	type:"get",
	dataType:"json",
	success:function(data,textStatus,jq){
        /*data、就是ajax的返回值、一个json对象*/
       
    },
	error:function(data,textStatus,jq){}
})
});
$("#getAllOrder").click(function(){
	 $.ajax({
	url:"Order/getAllOrder.action",
	
	type:"get",
	dataType:"json",
	success:function(data,textStatus,jq){
         /*data、就是ajax的返回值、一个json对象*/
        
     },
	error:function(data,textStatus,jq){}
})
});
$("#searchGoods").click(function(){
	 $.ajax({
	url:"Goods/searchGoods.action",
	
	type:"get",
	data:searchGoods,
	dataType:"json",
	success:function(data,textStatus,jq){
          /*data、就是ajax的返回值、一个json对象*/
         
      },
	error:function(data,textStatus,jq){}
})
});
$("#getUserRole").click(function(){
	 $.ajax({
	url:"Permission/getUserRole.action",
	
	type:"get",
	data:getUserRole,
	dataType:"json",
	success:function(data,textStatus,jq){
           /*data、就是ajax的返回值、一个json对象*/
          
       },
	error:function(data,textStatus,jq){}
})
});
$("#deleteUserRole").click(function(){
	 $.ajax({
	url:"Permission/deleteUserRole.action",
	
	type:"get",
	data:deleteUserRole,
	dataType:"json",
	success:function(data,textStatus,jq){
          /*data、就是ajax的返回值、一个json对象*/
         
      },
	error:function(data,textStatus,jq){}
})
});
$("#addUserRole").click(function(){
	 $.ajax({
	url:"Permission/addUserRole.action",
	
	type:"get",
	data:addUserRole,
	dataType:"json",
	success:function(data,textStatus,jq){
          /*data、就是ajax的返回值、一个json对象*/
         
      },
	error:function(data,textStatus,jq){}
})
});
$("#getRolePer").click(function(){
	 $.ajax({
	url:"Permission/getRolePer.action",
	
	type:"get",
	data:getRolePer,
	dataType:"json",
	success:function(data,textStatus,jq){
            /*data、就是ajax的返回值、一个json对象*/
           
        },
	error:function(data,textStatus,jq){}
})
});
$("#addRolePer").click(function(){
	 $.ajax({
	url:"Permission/addRolePer.action",
	
	type:"get",
	data:addRolePer,
	dataType:"json",
	success:function(data,textStatus,jq){
           /*data、就是ajax的返回值、一个json对象*/
          
       },
	error:function(data,textStatus,jq){}
})
});
$("#deleteRolePer").click(function(){
	 $.ajax({
	url:"Permission/deleteRolePer.action",
	
	type:"get",
	data:deleteRolePer,
	dataType:"json",
	success:function(data,textStatus,jq){
           /*data、就是ajax的返回值、一个json对象*/
          
       },
	error:function(data,textStatus,jq){}
})
});
$("#getAllRole").click(function(){
	 $.ajax({
 	url:"Permission/getAllRole.action",
 	
 	type:"get",
 	
 	dataType:"json",
 	success:function(data,textStatus,jq){
             /*data、就是ajax的返回值、一个json对象*/
            
         },
 	error:function(data,textStatus,jq){}
 })
});
$("#getAllPer").click(function(){
	 $.ajax({
 	url:"Permission/getAllPermission.action",
 	
 	type:"get",
 	dataType:"json",
 	success:function(data,textStatus,jq){
             /*data、就是ajax的返回值、一个json对象*/
            
         },
 	error:function(data,textStatus,jq){}
 })
});
$("#deleteRole").click(function(){
	 $.ajax({
  	url:"Permission/deleteRole.action",
  	
  	type:"get",
  	data:deleteRole,
  	dataType:"json",
  	success:function(data,textStatus,jq){
              /*data、就是ajax的返回值、一个json对象*/
             
          },
  	error:function(data,textStatus,jq){}
  })
});
$("#createReturn").click(function(){
	 $.ajax({
  	url:"SaleReturn/createSaleReturn.action",
  	
  	type:"get",
  	data:createReturn,
  	dataType:"json",
  	success:function(data,textStatus,jq){
              /*data、就是ajax的返回值、一个json对象*/
             
          },
  	error:function(data,textStatus,jq){}
  })
});
$("#addReturnItem").click(function(){
	 $.ajax({
   	url:"SaleReturn/addSaleReturnItem.action",
   	
   	type:"get",
   	data:addReturnItem,
   	dataType:"json",
   	success:function(data,textStatus,jq){
               /*data、就是ajax的返回值、一个json对象*/
              
           },
   	error:function(data,textStatus,jq){}
   })
});
$("#getRole").click(function(){
	 $.ajax({
   	url:"Permission/getRole.action",
   	
   	type:"get",
   	data:getRole,
   	dataType:"json",
   	success:function(data,textStatus,jq){
               /*data、就是ajax的返回值、一个json对象*/
              
           },
   	error:function(data,textStatus,jq){}
   })
});
$("#addRole").click(function(){
	 $.ajax({
   	url:"Permission/addRole",
   	
   	type:"get",
   	data:addRole,
   	dataType:"json",
   	success:function(data,textStatus,jq){
               /*data、就是ajax的返回值、一个json对象*/
              
           },
   	error:function(data,textStatus,jq){}
   })
});
$("#addUser").click(function(){
	 $.ajax({
    	url:"User/addUser.action",
    	
    	type:"get",
    	data:addUser,
    	dataType:"json",
    	success:function(data,textStatus,jq){
                /*data、就是ajax的返回值、一个json对象*/
               
            },
    	error:function(data,textStatus,jq){}
    })
});
	$("#addPer").click(function(){
		 $.ajax({
 	    	url:"Permission/addPermission.action",
 	    	
 	    	type:"get",
 	    	data:addPer,
 	    	dataType:"json",
 	    	success:function(data,textStatus,jq){
 	                /*data、就是ajax的返回值、一个json对象*/
 	               
 	            },
 	    	error:function(data,textStatus,jq){}
 	    })
	});
$("#getPer").click(function(){
	 $.ajax({
    	url:"Permission/getPermission.action",
    	
    	type:"get",
    	data:getPer,
    	dataType:"json",
    	success:function(data,textStatus,jq){
                /*data、就是ajax的返回值、一个json对象*/
               
            },
    	error:function(data,textStatus,jq){}
    })
});
$("#deletePer").click(function(){
	 $.ajax({
    	url:"Permission/deletePermission.action",
    	
    	type:"get",
    	data:deletePer,
    	dataType:"json",
    	success:function(data,textStatus,jq){
                /*data、就是ajax的返回值、一个json对象*/
               
            },
    	error:function(data,textStatus,jq){}
    })
});
    $("#goodsSearch").click(function(){
    	 $.ajax({
    	    	url:"Goods/addGoods.action",
    	    	
    	    	type:"get",
    	    	data:goods,
    	    	dataType:"json",
    	    	success:function(data,textStatus,jq){
    	                /*data、就是ajax的返回值、一个json对象*/
    	               
    	            },
    	    	error:function(data,textStatus,jq){}
    	    })
    });
    $("#createOrder").click(function(){
   	 $.ajax({
   	    	url:"Goods/getAllGoodsType.action",
   	    	type:"get",
   	    	
   	    	dataType:"json",
   	    	success:function(data,textStatus,jq){
   	                /*data、就是ajax的返回值、一个json对象*/
   	               
   	            },
   	    	error:function(data,textStatus,jq){
   	    		alert(textStatus);
   	    		
   	    		alert(data+"is data");
   	    		alert(jq);
   	    	}
   	    })
   });
    $("#createOrderItem").click(function(){
      	 $.ajax({
      	    	url:"Order/createOrderItem.action",
      	    	type:"get",
      	    	data:orderItem,
      	    	dataType:"json",
      	    	success:function(data,textStatus,jq){
      	               alert("SUCCESS");
      	            },
      	    	error:function(data,textStatus,jq){
      	    		alert(textStatus);
      	    		
      	    		alert(data+"is data");
      	    		alert(jq);
      	    	}
      	    })
      });
   
    </script>
</html>
