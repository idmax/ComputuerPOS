

	// 对表格的行进行隔行显示灰色
	$("table tr:not(:has(th)):odd").css({"background-color":"#F7F8FA"});


	//对表格进行选中
	var $table = $("table");
	var $tr = $("table tr");

	$("table").on("click","tr",function(){
		$(this).toggleClass("selected");
	})

	var $selected = $(".selected");


	//键盘控制
	$(".bottom,.table").keydown(function(event){
		var KeyCode = {
			del:46,
			enter:13
		};
		if (event.which == KeyCode.del) {
			$(".selected").each(function(){
				var price = parseFloat($("#totalPrice span").text()) - parseFloat($(this).find("td").eq(6).text());
				var number = parseInt($("#total span").text()) - parseInt($(this).find("td").eq(4).text());
				$("#totalPrice span").text(price);
				$("#total span").text(number);
				$(this).remove();
			});
			trNum();
		}
		else if (event.which == KeyCode.enter) {
			$("#add").click();
		};
	});
	$(document).keydown(function(event){
		var KeyCode = {
			del:46,
			enter:13
		};
		if (event.which == KeyCode.del) {
			$(".selected").each(function(){
				if (confirm("真的要删除该商品吗？")) {
					var price = parseFloat($("#totalPrice span").text()) - parseFloat($(this).find("td").eq(6).text());
					var number = parseInt($("#total span").text()) - parseInt($(this).find("td").eq(4).text());
					$("#totalPrice span").text(price.toFixed(2));
					$("#total span").text(number);
					$(this).remove();
				};
			});
			trNum();
		}
	});

 	//重新对序号进行编号
 	function trNum(){
 		var $firstTd = $("tr td:first-child");
 		var num = 1;
 		var i = 0;
 		$firstTd.each(function(){
 			$(this).text(num);
 			num++;
 		})
 	}

 	//清空销售临时商品表格数据
 	$(".cancel").click(function(){
 		$(".table table").empty();
 	})
 	
	//设置当前时间
	function time(){
		var date = new Date();
		var year = date.getFullYear();
		var month = date.getMonth();
		var day = date.getDate();
		var hour = date.getHours();
		var minute = date.getMinutes();
		var second = date.getSeconds();
		var $time = $("time");
		var txt = year + "年" + (month + 1) + "月" + day + "日  " + hour + ":" + minute + ":" + second;
		$time.text(txt); 
	}
	
	setInterval("time()",1000);

	//设置订单号
	var orderNum = "";
	//初始化支付号
	var paymentNum = "";
	//设置退货号
	var returnNum = "";

	var dayNum = Math.floor(Math.random() * 200 + 1);
	if (dayNum<10) {
		dayNum = '00' + dayNum;
	}
	else if (10 <=dayNum && dayNum <= 99) {
		dayNum = '0' +dayNum;
	};

	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	if (month<10) {
		month = '0' + month;
	};
	if (day<10) {
		day = '0' + day;
	};


	orderNum = "FM" + year + month + day + dayNum;
	paymentNum = "PM" + year + month + day + dayNum;
	returnNum = "RM" +  year + month + day + dayNum;
	// alert(orderNum);
	$("#orderId").text(orderNum);
	$(".orderNum").text(orderNum);
	$(".paymentNum").text(paymentNum);
	$("#returnNum").text(returnNum);


//显示用户信息
if ( sessionStorage.getItem('userName') == '' || sessionStorage.getItem('userName') == null || sessionStorage.getItem('userName') == undefined ) {

	// alert("抱歉，你还没登陆呢~要先登陆哈~")
	// window.location.href="../login.html";
}
$(".name").text(sessionStorage.getItem('userName'));
$(".name").click(function(){
	window.location.href="../updateUser.html";
});
//退出
$(".exit").click(function(){
	sessionStorage.setItem('userNumber','');
	sessionStorage.setItem('userName','');
	window.location.href="../login.html";
})