
var userNumber = sessionStorage.getItem('userNumber');
var data = {
		"user.UserNumber":userNumber
}
var userId = '';
UserAPI.searchUserAPI(data,
	function(json){
		$.each(json,function(i,val){
			$("#update_userName").val(val.userName);
			$("#update_userPasswd").val(val.userPasswd);
			$("#update_userTel").val(val.tel);
			userId = val.userId;
		})
	},function(){

	})

$("#updateUser").click(function(){
	var userName = $("#update_userName").val();
	var userPassword = $("#update_userPasswd").val();
	var tel = $("#update_userTel").val();
	var gender = $(".update_main .gender_option").val();
	if( userName == '' || userPassword == '' || tel == '' ){
		alert()
	}
	var data ={
	    	"user.userId":parseInt(userId),
	    	"user.userName":userName,
	    	"user.userPassword":userPassword,
	    	"user.tel":tel,
	    	"user.gender":gender
	    }
	    UserAPI.updateUserAPI(data,
	    	function(){
	    		alert("修改成功");
	    		sessionStorage.setItem('userName',userName);
	    		window.history.go(-1);
	    	},
	    	function(){
	    		alert("修改失败,请重试");
	    	})
})