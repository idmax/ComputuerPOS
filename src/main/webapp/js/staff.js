
//收银员管理（经理使用）
//获取所有商品
function getAllstaff(){
	var data = "";
	var tr; //行数据
	var $table = $("table");
	UserAPI.getAllUserAPI(data,
		function(json){
			//清除原数据
			$("table tr").each(function(i,val){
				if (i != 0) {
					val.remove()
				};
			});
			$.each(json,function(i,val){
				// alert(val.userId);
				var userId = parseInt(val.userId);
				var userNumber = val.userNumber;
				var userName = val.userName;
				var userGender = val.gender;
				var userTel ;
				if (val.tel == null) {
					userTel = "";
				}else{
					userTel = val.tel;
				}
				// alert(userId);
				// if (userId) {};
				var data = { "userRole.userId":userId };
				relateAPI.getUserRoleAPI(data,
					function(json){
						// alert(json)
						$.each(json,function(i,val){
							if (val.roleId == 3 || val.roleId == "3" ) {
								// alert(val.roleId);
								tr ='<tr><td>1</td><td class="none_id">'+
									userId+'+</td><td>'+
									userNumber+'</td><td>'+
									userName+'</td><td>'+
									userGender+'</td><td>收银员</td><td>'+
									userTel+'</td><td><button class="alter">修改</button><button class="detect">删除</button></td></tr>'
								$table.append(tr);
							};
						})
						trNum();
					},
					function(){

					})
				
				// alert(tr);
				// $table.append(tr);
			})
			trNum();
		},
		function(){

		})
}

//获取所有的类型
// function getAllGoodsType(){
// 	var data = "";
// 	var option; //选项
// 	var $select = $(".type_option");
// 	typeAPI.getAllGoodsTypeAPI(data,
// 		function(json){
// 			//清除原数据
// 			$select.empty();
// 			$.each(json,function(i,val){
// 				option = '<option value="'+val.typeId+'">'+val.typeName+'</option>';
// 				// alert(tr);
// 				$select.append(option);
// 			})
// 		},
// 		function(){

// 		})
// }
// getAllGoodsType();
//添加按钮
$(".add-btn").click(function(){
	$(".addStaff_page").css("display","block");
	// getAllstaff();

});
//关闭弹出窗口
$(".close_icon").click(function(){
	$(".addStaff_page").css("display","none");
	$(".updateStaff_page").css("display","none");
})

$("#addStaff").click(function(){
	var staffName = $("#add_staffName").val();
	var staffGender = $(".gender_option").val();
	var staffRoleId = 3;
	var staffNum = parseInt($("table tr:last-child").find("td").eq(2).text()) + 1;
	// alert(goodsType);
	if (staffName == "" || staffGender == "" ) {
		alert("您填写的信息不完整，请检查填写~");
	}
	else{
		var data = {
		"user.UserNumber":staffNum,
		"user.UserName":staffName,
		"user.Gender":staffGender,
		"userRole.roleId":staffRoleId
		}
		UserAPI.addUserAPI(data,
			function(){
				alert("已成功添加员工");
				$(".addStaff_page").css("display","none");
				getAllstaff();
			},
			function(){
				alert("添加失败，请重新添加");
			})
	}
	
});


//修改收银员
var temp_staffId = "";
$("table").on('click','tr td .alter',function() {
	
    $(".updateStaff_page").css("display","block");
    var staffId = $(this).parent().parent().find("td").eq(1).text();
    var staffNum = $(this).parent().parent().find("td").eq(2).text();
    var staffName = $(this).parent().parent().find("td").eq(3).text();
    var staffTel = $(this).parent().parent().find("td").eq(6).text();

    $("#update_StaffName").val(staffName);
    $("#update_StaffTel").val(staffTel);
    $("#update_StaffPasswd").val("1234");
    temp_staffId = staffId;
});
$("#updateStaff").click(function(){

	var staffId = temp_staffId;
    var staffName = $("#update_StaffName").val();
    var staffTel = $("#update_StaffTel").val();
    var staffPasswd = $("#update_StaffPasswd").val();
    var staffGender = $(".updatestaff_page .gender_option").val();

	if (staffName == "" || staffPasswd == ""  || staffTel == "" ) {
		alert("您填写的信息不完整，请检查填写~");
	}
	else{
	    var data ={
	    	"user.userId":parseInt(staffId),
	    	"user.userName":staffName,
	    	"user.userPassword":staffPasswd,
	    	"user.tel":staffTel,
	    	"user.gender":staffGender
	    }
	    UserAPI.updateUserAPI(data,
	    	function(){
	    		alert("修改成功");
	    		$(".updateStaff_page").css("display","none");
	    		getAllstaff();
	    	},
	    	function(){
	    		alert("修改失败");
	    	})
	}
})

//删除收银员
$("table").on('click',"tr td .detect",function(){
	var staffNum = $(this).parent().parent().find("td").eq(2).text();
	var data = {
		"user.UserNumber":staffNum
	}
	alert
	UserAPI.deleteUserAPI(data,
		function(){
			alert("删除成功");
			getAllstaff();
		},
		function(){
			alert("删除失败");
		})
})

//搜索商品
