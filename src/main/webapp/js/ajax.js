
	//对ajax进行第一轮的封装
	function myAjax(opera, url, data, callback1, callback2,isAsync){
		var async = (typeof isAsync != 'undefined')? isAsync:true;
		$.ajax({
			async:async,
			url:url,
			type:opera,
			data:data,
			contentType:'application/x-www-form-urlencoded; charset=UTF-8',
			success:function(json){
				if (callback1 != null&& callback1 !='') {
					callback1(json);
				}
			},
			error:function(json){
				if (callback2 != null && callback2 != '') {
					// alert("id不存在");
				};
			}
		})
	}

//商品api
var goodsAPI = {
	//获取单个商品API
	getGoodsAPI: function(data, callback1, callback2) {
		var url ='Goods/searchGoods.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//获取所有的商品API
	getAllGoodsAPI: function(data, callback1, callback2) {
		var url ='Goods/getAllGoods.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//添加商品API
	addGoodsAPI: function(data, callback1, callback2) {
		var url ='Goods/addGoods.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//删除商品API
	deleteGoodsAPI: function(data, callback1, callback2) {
		var url ='Goods/deleteGoods.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//更新商品API
	updateGoodsAPI: function(data, callback1, callback2) {
		var url ='Goods/updateGoods.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//通过商品名获取商品（支持模糊查询）API
	getGoodsByGoodsNameAPI: function(data, callback1, callback2) {
		var url ='Goods/getGoodsByGoodsName.action';
		myAjax('get', url, data, callback1, callback2);
	}

}

//商品类型API
var typeAPI = {

	//添加商品类型API
	addTypeAPI: function(data, callback1, callback2) {
		var url ='Goods/addType.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//获取所有商品类型API
	getAllGoodsTypeAPI: function(data, callback1, callback2) {
		var url ='Goods/getAllGoodsType.action';
		myAjax('get', url, data, callback1, callback2,false);
	},
	//更新商品类型API
	updateTypeAPI: function(data, callback1, callback2) {
		var url ='Goods/updateType.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//删除商品类型API
	deleteTypeAPI: function(data, callback1, callback2) {
		var url ='Goods/deleteType.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//通过ID获取商品类型API
	getTypeByIdAPI: function(data, callback1, callback2) {
		var url ='Goods/getTypeById.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//通过商品类型名获取商品类型（模糊）API
	getTypeByTypeNameAPI: function(data, callback1, callback2) {
		var url ='Goods/getTypeByTypeName.action';
		myAjax('get', url, data, callback1, callback2);
	}
}

//用户api
var UserAPI = {
	//获取单个用户信息API （登陆）
	searchUserAPI: function(data, callback1, callback2) {
		var url ='User/searchUser.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//获取所有的用户API
	getAllUserAPI: function(data, callback1, callback2) {
		var url ='User/getAllUser.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//添加用户API
	addUserAPI: function(data, callback1, callback2) {
		var url ='User/addUser.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//删除用户API
	deleteUserAPI: function(data, callback1, callback2) {
		var url ='User/deleteUser.action';
		myAjax('get', url, data, callback1, callback2,false);
	},
	//更新用户API
	updateUserAPI: function(data, callback1, callback2) {
		var url ='User/updateUser.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//通过用户名获取用户信息（模糊）API
	getUserByNameAPI: function(data, callback1, callback2) {
		var url ='User/getUserByName.action';
		myAjax('get', url, data, callback1, callback2);
	}

}

//订单api
var orderAPI = {
	//创建订单API
	setorderAPI: function(data, callback1, callback2) {
		var url ='Order/createOrder.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//添加订单项API
	createOrderItemAPI: function(data, callback1, callback2){
		var url = 'Order/createOrderItem.action'
		myAjax('get',url,data,callback1,callback2);
	},
	//添加支付API
	createPaymentAPI:function(data, callback1, callback2){
		var url = 'Payment/createPayment.action'
		myAjax('get',url,data,callback1,callback2);
	},
	//通过订单号获取订单信息API
	getOrderByNumberAPI:function(data, callback1, callback2){
		var url = 'Order/getOrderByNumber.action'
		myAjax('get',url,data,callback1,callback2);
	},
	//获取所有订单API
	getAllOrderAPI: function(data, callback1, callback2){
		var url = 'Order/getAllOrder.action'
		myAjax('get',url,data,callback1,callback2,false);
	},
	//添加订单项API
	getAllOrderItemAPI:function(data, callback1, callback2){
		var url = 'Order/getAllOrderItem.action'
		myAjax('get',url,data,callback1,callback2,false);
	},
	//通过通过ID获取所有订单项API
	getOrderItemByOrderIdAPI:function(data, callback1, callback2){
		var url = 'Order/getOrderItemByOrderId.action'
		myAjax('get',url,data,callback1,callback2);
	},
	//获取指定时间段的订单API
	getOrderByTimeAPI: function(data, callback1, callback2){
		var url = 'Order/getOrderByTime.action'
		myAjax('get',url,data,callback1,callback2);
	},
	//获取指定的时间段的订单项API
	getOrderItemByTimeAPI:function(data, callback1, callback2){
		var url = 'Order/getOrderItemByTime.action'
		myAjax('get',url,data,callback1,callback2);
	},
	//通过用户名获取用户的订单API
	getOrderByUserNameAPI:function(data, callback1, callback2){
		var url = 'Order/getOrderByUserName.action'
		myAjax('get',url,data,callback1,callback2);
	}
}

//退款api
var saleReturnAPI = {
	//创建退款API
	createSaleReturnAPI: function(data, callback1, callback2) {
		var url ='SaleReturn/createSaleReturn.action';
		myAjax('get', url, data, callback1, callback2,false);
	},
	//添加退款项API
	addSaleReturnItemAPI: function(data, callback1, callback2){
		var url = 'SaleReturn/addSaleReturnItem.action'
		myAjax('get',url,data,callback1,callback2,false);
	}
}

//角色API
var roleAPI = {
	//获取单个角色信息API （登陆）
	getRoleAPI: function(data, callback1, callback2) {
		var url ='Role/getRole.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//获取所有的角色API
	getAllRoleAPI: function(data, callback1, callback2) {
		var url ='Permission/getAllRole.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//添加角色API
	addRoleAPI: function(data, callback1, callback2) {
		var url ='Role/addRole.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//删除角色API
	deleteRoleAPI: function(data, callback1, callback2) {
		var url ='Role/deleteRole.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//更新角色API
	updateRoleAPI: function(data, callback1, callback2) {
		var url ='Permission/updateRole.action';
		myAjax('get', url, data, callback1, callback2);
	}

}

//权限API
var permissionAPI = {
	//获取单个权限信息API （登陆）
	getPermissionAPI: function(data, callback1, callback2) {
		var url ='Permission/getPermission.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//获取所有的权限API
	getAllPermissionAPI: function(data, callback1, callback2) {
		var url ='Permission/getAllPermission.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//添加权限API
	addPermissionAPI: function(data, callback1, callback2) {
		var url ='Permission/addPermission.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//删除权限API
	deletePermissionAPI: function(data, callback1, callback2) {
		var url ='Permission/deletePermission.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//更新权限API
	updatePermissionAPI: function(data, callback1, callback2) {
		var url ='Permission/updatePermission.action';
		myAjax('get', url, data, callback1, callback2);
	}

}

//关联API
var relateAPI = {
	
	//获取所有角色-权限API
	getRolePerAPI: function(data, callback1, callback2) {
		var url ='Permission/getRolePer.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//添加角色-权限关联API
	addRolePerAPI: function(data, callback1, callback2) {
		var url ='Permission/addRolePer.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//删除角色-权限关联API
	deleteRolePerAPI: function(data, callback1, callback2) {
		var url ='Permission/deleteRolePer.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//添加用户-角色关联API
	addUserRoleAPI: function(data, callback1, callback2) {
		var url ='Permission/addUserRole.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//删除用户-角色关联API
	deleteUserRoleAPI: function(data, callback1, callback2) {
		var url ='Permission/deleteUserRole.action';
		myAjax('get', url, data, callback1, callback2);
	},
	//通过Userid获取角色-权限API
	getUserRoleAPI: function(data, callback1, callback2) {
		var url ='Permission/getUserRole.action';
		myAjax('get', url, data, callback1, callback2,false);
	}

}


//测试接口
// function test(){
// 	alert("我是测试");
// 	var name="洗发露";
// 	var data ={
// 		"goods.goodsNumber":1003,
// 		"goods.GoodsName":"洗发露",
// 		"goods.Price":28,
// 		"goods.type.typeId":1
// 	} ;
// 	goodsAPI.addGoodsAPI(data,
// 		function(json){
// 			alert(json);
// 		},
// 		function(){
// 			alert("失败");
// 		})
// }
// test();
// function test1(){
// 	alert("我是测试");
// 	var data ;
// 	UserAPI.getAllUserAPI(data,
// 		function(json){
// 			alert(json);
// 		},
// 		function(){
// 			alert("失败");
// 		})
// }
// function deleteGoods(){
// 	alert("我是测试");
// 	var name="洗发露";
// 	var data = {
// 		"goods.goodsNumber":1003
// 	} ;
// 	goodsAPI.deleteGoodsAPI(data,
// 		function(json){
// 			alert(json);
// 		},
// 		function(){
// 			alert("失败");
// 		})
// }

// function searchUser(){
// 	alert("我是测试");
// 	var data = {
// 		"user.UserNumber":"001"
// 	} ;
// 	UserAPI.searchUserAPI(data,
// 		function(json){
// 			// alert(json);
// 		},
// 		function(){
// 			alert("失败");
// 		})
// }
// function addUser() {
// 	var name="洗发露";
// 	var data ={
// 		"user.UserNumber":"1006",
// 		"user.UserName":"小米",
// 		"user.Gender":"男"
// 	} ;
// 	UserAPI.addUserAPI(data,
// 		function(json){
// 			alert(json);
// 		},
// 		function(){
// 			alert("失败");
// 		})
// }
// function deleteUser(){//成功
// 	alert("我是测试");
// 	var name="洗发露";
// 	var data = {
// 		"user.UserNumber":"1006"
// 	} ;
// 	UserAPI.deleteUserAPI(data,
// 		function(json){
// 			alert(json);
// 		},
// 		function(){
// 			alert("失败");
// 		})
// }


// function test1(){
// 	alert("我是测试");
// 	var data ;
// 	UserAPI.getAllUserAPI(data,
// 		function(json){
// 			alert(json);
// 		},
// 		function(){
// 			alert("失败");
// 		})
// }
// //退款
// function getOrderByNumber(){
// 	alert("我是测试");
// 	var data ={
// 		"order.orderNumber":"FM20150415001"
// 	};
// 	orderAPI.getOrderByNumberAPI(data,
// 		function(json){
// 			// alert(json);
// 		},
// 		function(){
// 			alert("失败");
// 		})
// }
// function getOrderByNumber(){
// 	alert("我是测试");
// 	var data ={
// 		"order.orderNumber":"FM2010415001"
// 	};
// 	orderAPI.getOrderByNumberAPI(data,
// 		function(json){
// 			// alert(json);
// 		},
// 		function(){
// 			alert("失败");
// 		})
// }
// function createSaleReturn(){
// 	alert("我是测试");
// 	var data ={
// 		"saleReturn.salereturnNo":"TM56565",
// 		"saleReturn.order.orderId":27,
// 		"saleReturn.reason":"退货原因",
// 		"saleReturn.total":100.50
// 	};
// 	saleReturnAPI.createSaleReturnAPI(data,
// 		function(json){
// 			// alert(json);
// 		},
// 		function(){
// 			alert("失败");
// 		})
// }
// function addSaleReturnItem(){
// 	alert("我是测试");
// 	var data ={
// 		"saleReturnItem.salereturn.id":1,
// 		"saleReturnItem.goods.GoodsId":1,
// 		"saleReturnItem.quantity":1,
// 		"saleReturnItem.price":12.5
// 	};
// 	saleReturnAPI.addSaleReturnItemAPI(data,
// 		function(json){
// 			// alert(json);
// 		},
// 		function(){
// 			alert("失败");
// 		})
// }
// $(".add-btn").click(function(){
// 	// getOrderByNumber();
// 	// addSaleReturnItem();
// 	// searchUser();
// 	createSaleReturn()
// })