
//通过订单号获取订单信息
$(".search-btn").click(function(){
	var orderNum = $(".orderNum").val();
	var tr = "";
	var $table = $("table");
	if (orderNum != "") {
		// alert(orderNum);
		var data ={
			"order.orderNumber":orderNum
		}
		orderAPI.getOrderByNumberAPI(data,
			function(json){
				$.each(json,function(i,val){
					$("#order_id").text(val.orderId);
					var orderId ={ "order.orderId":val.orderId };
					orderAPI.getOrderItemByOrderIdAPI(orderId,
						function(json){
							//清除原数据
							$("table tr").each(function(i,val){
								if (i != 0) {
									val.remove()
								};
							});
							$.each(json,function(i,val){
								var count = parseFloat(val.price) * parseInt(val.saleNumber);
								tr = '<tr><td>1</td><td>'+
									orderNum+'</td><td>'+
									val.goods.goodsName+'</td><td>'+
									val.price.toFixed(2)+'</td><td>'+
									val.saleNumber+'</td><td>'+
									count.toFixed(2)+'</td><td><input type="checkbox" class="isSelect" value="'+
									val.goods.goodsId+'" /> </td><td class="none_id">'+
									val.orderItemId+'</td></tr>';
								$table.append(tr);
							})
							trNum();//对序号进行顺序编号
						},function(){})
				})
				
			},
			function(){
				alert("系统繁忙，请重试")
			})
	}
	else{
		alert("请先填写订单号")
	};
})

//全选，取消全选
$("#checkAll").click(function(){  
    if (this.checked) {  
        $("input[class='isSelect']").each(function() { //遍历所有的name为selectFlag的 checkbox  
                    $(this).attr("checked",true);
                })  
    } else {   //反之 取消全选   
        $("input[class='isSelect']").each(function(){ //遍历所有的name为selectFlag的 checkbox  
                    $(this).attr("checked",false);;  
                })  
    }  
});

//对已选进行退款
$(".return_btn").click(function(){

 	 $(".return_page").css("display","block"); //显示页面
 	 var orderNum = $("table").find("tr").eq(1).find("td").eq(1).html();//获取orderNum
 	 $("#orderNum").text(orderNum);

 	 //计算退款金额
 	 var count = 0;//退款总金额
 	 $("input[class='isSelect'][checked]").each(function() { //遍历所有的name为selectFlag的 checkbox  
           // alert($(this).val());
           count = count + parseFloat($(this).parent().parent().find("td").eq(5).text());
           // alert(count);

     })
     $(".returnMon").text(count.toFixed(2));
     // $(".isSelect[checked]").each(function() { //遍历所有的name为selectFlag的 checkbox  
     //       alert($(this).val());

     // })

})
//关闭弹出窗口
$(".close_icon").click(function(){
	$(".return_page").css("display","none");
})

$("#sureReturn").click(function(){
	// $("table").on("click","tr ",function(){
	// 	alert($(this).html());
	// })
	var salereturnNo = $("#returnNum").text();
	var reason = $("#reason").val();
	var total = parseFloat($(".returnMon").text());
	var orderId = parseInt($("#order_id").text());

	var data = {
		"saleReturn.salereturnNo":salereturnNo,
		"saleReturn.order.orderId":orderId,
		"saleReturn.reason":reason,
		"saleReturn.total":total
	}
	saleReturnAPI.createSaleReturnAPI(data,
		function(json){
			var returnId = json.id;
			$("input[class='isSelect'][checked]").each(function() { //遍历所有的name为selectFlag的 checkbox
				   var goodsId = $(this).val();
				   var saleNumber = parseInt($(this).parent().parent().find("td").eq(4).text());
				   var price = parseFloat($(this).parent().parent().find("td").eq(5).text());
				   var orderItemId = parseInt($(this).parent().parent().find("td").eq(7).text());
		           // count = count + parseFloat($(this).parent().parent().find("td").eq(5).text());
		           // alert(count);
		           var data = {
		           		"saleReturnItem.salereturn.id":returnId,
		           		"saleReturnItem.goods.GoodsId":goodsId,
		           		"saleReturnItem.quantity":saleNumber,
		           		"saleReturnItem.price":price,
		           		"orderItem.orderItemId":orderItemId
		           }

		           saleReturnAPI.addSaleReturnItemAPI(data,
		           		function(){

		           		},function(){

		           		});
     		})
     		alert("退款成功~")
     		window.location.href="../return.html";
		},
		function(){
			alert("退款失败，请重试~")
		})
})