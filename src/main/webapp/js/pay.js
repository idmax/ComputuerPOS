$(document).ready(function(){

	//添加商品
	$("#add").click(function(){
		var goodsId = $("#goodsId").val();
		if (goodsId == '') {
			alert("请先输入商品号");
		}else{
			var goods = {
				"goods.goodsNumber":goodsId
			};
			goodsAPI.getGoodsAPI(goods,
				function(json){
					if (json == "") {
						alert("该商品号不存在，请检查输入");
					}else{
					var $table = $("table");
					var tr = "";
					var goodsNum = "";
					var goodsName = "";
					var discount = "";
					var price = "";
					var quantity = parseInt($("#goodsNum").val());
					var amount = 0;
					$.each(json,function(i,val){
						// switch(i){
						// 	case "goodsNumber": goodsNum = val; break;
						// 	case "goodsName":  goodsName = val; break;
						// 	case "discount":  discount = val; break;
						// 	case "price":  price = val; break;
						// 	case "goodsId": goodsId = val; break;
						// }
						goodsID = val.goodsId;
						goodsNum = val.goodsNumber;
						goodsName = val.goodsName;
						discount = parseFloat(val.discount);
						price = parseFloat(val.price);
						amount = price * discount * quantity; //单类商品总价
						tr = '<tr><td>1</td><td>' + goodsNum +'</td><td>' + goodsName +'</td><td>' + price +'</td><td>' + quantity +
						'</td><td>' + discount +'</td><td>' + amount +'</td><td class="none">'+goodsID+'</td></tr>';
						$table.append(tr);  //向table追加一行商品数据
					})


				//修改页面的订单总金额及总数量
				var $totalPrice = $("#totalPrice span");
				var $total = $("#total span");
				var totalPrice = $totalPrice.text();
				var total = $total.text();
				totalPrice = parseFloat(totalPrice) + amount;
				total = parseFloat(total) + parseFloat(quantity);
				$totalPrice.text(totalPrice.toFixed(2));
				$total.text(total);
				//对序号进行重新编号
				trNum();
				// addOrderItem(orderId);					
			};

			},
			function(){
				alert("抱歉~没有该商品，请检查ID重新输入");
			})
		}
		
})


//创建订单
$(".button .pay").click(function(){

	var orderId = $("#orderId").text();
	var time = $("time").html();
	var total = $("#total span").text();
	var totalPrice = $("#totalPrice span").text();
	var orderInfo = "*****已成功下单******\n订单号："+ orderId +"\n下单时间："+ 
	time + "\n总商品数：" + total + 
	"\n总金额: " + totalPrice;
	var order = {
		"order.users.userId":1,
		"order.orderNumber":orderId,
		"order.totalPrice":totalPrice
	}
	orderAPI.setorderAPI(order,
		function(json){
		 		addOrderItem(parseInt(json.orderId));
		 		sessionStorage.setItem('orderId',orderId);
		 	},
		 	function(){
		 		alert("下单失败，请重新下单");
		 	}
		 	)

});

//添加订单项

function addOrderItem(orderId){
	var $tr = $("table tr");
	var $td = $("td");
	// var orderId1 = orderId;//订单号
	// var saleNumArr = [];//商品数量
	var saleNum='';
	// var goodsNumArr = [];//商品号
	var goodsId= 0;
	for (var i = 1; i < $tr.length; i++) {
		saleNum = $("table").find("tr").eq(i).find("td").eq(4).text();
		goodsId = parseInt($("table").find("tr").eq(i).find("td").eq(7).text());
		// alert(goodsId);
		// saleNumArr.push(saleNum);
		// goodsNumArr.push(goodsNum);
		var data = {
			"orderItem.saleNumber":saleNum,
			"orderItem.goods.goodsId":goodsId,
			"orderItem.order.orderId":orderId
		}
		orderAPI.createOrderItemAPI(data,
			function(){
				// alert("订单项添加成功");
			},
			function(){
				// alert("s")
			})
		// alert(saleNum + "dd" +goodsId);
		$(".wrap").css("display","block");
		$(".payed").focus();
		var totalPrice = $("#totalPrice span").text();
		$(".totalPrice").text(totalPrice);
	};
	
	
}
// addOrderItem();
$(".sure").click(function(){
	createPayment();
})
function createPayment(){
	var userId = 1;
	var paymentNum = $(".paymentNum").text();
	// alert(paymentNum);
	var orderId = sessionStorage.getItem('orderId');
	var data = {
		"payment.paymentNo":paymentNum,
		"payment.users.userId":userId,
		"payment.order.orderId":orderId
	}
	orderAPI.createPaymentAPI(data,
		function(){
			alert("已支付成功，欢迎再次光临");
			window.location.href="index.html";
		},
		function(){
			alert("支付失败");
		})
}


	//显示最终支付交易界面
	var $wrap = $(".wrap");
	var $payed = $("#payed"); //实付
	var $totalPrice = $(".totalPrice"); //总计金额
	var $exact = $(".exact"); //找赎
	var $sure =$(".sure");

	
	//close支付窗口
	$(".close").click(function(){
		$wrap.css("display","none");
	})

	//自动显示找赎
	$payed.blur(function(){
		var totalPrice = $totalPrice.text();
		var payed = $("#payed").val();
		var exact = payed - totalPrice;
		$exact.text(exact.toFixed(2));
	})
	
	$payed.keydown(function(event){
		//按回车键或者“=”键进行显示找赎
		if (event.which == 187 || event.which == 13) {
			$payed.blur();
		};
	})

	//取消支付
	$(".cancelPay").click(function(){
		$wrap.css("display","none");
	});

	//确定支付
	// $sure.click(function(){
	// 	alert("已完成订单，返回进行下一次订单");

	// })

});