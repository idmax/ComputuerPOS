
//各个月份销售总额初始化；
var Jan = 0,
    Feb = 0, 
    Mar = 0, 
    Apr =  0,
    May = 0,
	Jun = 0,
	Jul = 0,
	Aug = 0, 
    Sept= 0, 
    Oct = 0,
	Nov = 0,
    Dec =0;

orderAPI.getAllOrderAPI('',
	function(json){
		$.each(json,function(i,val){
			var date = new Date(val.orderTime);
			var month = date.getMonth() + 1;
			switch(month){
				case 1 : Jan += val.totalPrice; break;
				case 2 : Feb += val.totalPrice; break;
				case 3 : Mar += val.totalPrice; break;
				case 4 : Apr += val.totalPrice; break;
				case 5 : May += val.totalPrice; break;
				case 6 : Jun += val.totalPrice; break;
				case 7 : Jul += val.totalPrice; break;
				case 8 : Aug += val.totalPrice; break;
				case 9 : Sept += val.totalPrice; break;
				case 10 : Oct += val.totalPrice; break;
				case 11 : Nov += val.totalPrice; break;
				case 12 : Dec += val.totalPrice; break;
			}
			// var totalPrice = val.totalPrice;
			
		})
		// alert(May + "$$" + Jun);
	},
	function(){
		alert("加载失败，请刷新看看")
	})

//获取类型
var one = 0, two = 0, three = 0 ,four = 0,five = 0, six = 0;
var typeNameArr = [];
typeAPI.getAllGoodsTypeAPI('',
		function(json){
			$.each(json,function(i,val){
				typeNameArr.push(val.typeName);
			})
		},
		function(){
			alert("加载失败，请刷新看看")
		})

//获取订单项统计商品类别销售量
orderAPI.getAllOrderItemAPI('',
	function(json){
		$.each(json,function(i,val){
			var typeId = val.goods.type.typeId;
			var count = parseInt(val.saleNumber) * parseFloat(val.price) * parseFloat(val.discount);
			switch(typeId){
				case 1 : one += count ;break;
				case 2 : two += count ;break;
				case 3 : three += count ;break;
				case 4 : four += count ;break;
				case 5 : five += count ;break;
				case 6 : six += count ;break;
			}
		})
		// alert(one+"$$"+two);
	},
	function(){
		alert("加载失败，请刷新看看")
	});

// alert(one+"$$"+two);
// alert(typeNameArr);

//按时间段进行查看商品类别销售量
$("#searchTime").click(function(){
	var start = $("#start_time").val();
	var end = $("#end_time").val();
	var data = {
		"start":start,
		"end":end
	}
	orderAPI.getOrderItemByTimeAPI(data,
		function(json){
			one = 0; two = 0; three = 0 ;four = 0;five = 0; six = 0;
			$.each(json,function(i,val){
				var typeId = val.goods.type.typeId;
				var count = parseInt(val.saleNumber) * parseFloat(val.price) * parseFloat(val.discount);
				switch(typeId){
					case 1 : one += count ;break;
					case 2 : two += count ;break;
					case 3 : three += count ;break;
					case 4 : four += count ;break;
					case 5 : five += count ;break;
					case 6 : six += count ;break;
				}

			})
			var pieData = [
				{
					value: one,
					color:"#F7464A",
					highlight: "#FF5A5E",
					label: typeNameArr[0]
				},
				{
					value: two,
					color: "#46BFBD",
					highlight: "#5AD3D1",
					label: typeNameArr[1]
				},
				{
					value: three,
					color: "#FDB45C",
					highlight: "#FFC870",
					label: typeNameArr[2]
				}

			];

			var ctx1 = document.getElementById("canvas").getContext("2d");
			ctx1.fillStyle="#F1F2F6";
			window.myPie = new Chart(ctx1).Pie(pieData);
		},
		function(){
			alert("加载失败，请刷新看看")
		})
})

//月销售量图表

	var barChartData = {
		labels : ["January","February","March","April","May","June","July"],
		datasets : [
			{
				fillColor : "rgba(6,157,212,0.5)",
				strokeColor : "rgba(220,220,220,0.8)",
				highlightFill: "rgba(7,213,165,0.5)",
				highlightStroke: "rgba(220,220,220,1)",
				data : [Jan,Feb,Mar,Apr,May,Jun,Jul]
			}
		]

	}

// 商品类型统计表
var pieData = [
				{
					value: one,
					color:"#F7464A",
					highlight: "#FF5A5E",
					label: typeNameArr[0]
				},
				{
					value: two,
					color: "#46BFBD",
					highlight: "#5AD3D1",
					label: typeNameArr[1]
				},
				{
					value: three,
					color: "#FDB45C",
					highlight: "#FFC870",
					label: typeNameArr[2]
				}

			];


	window.onload = function(){
		var ctx = document.getElementById("chart-area").getContext("2d");
		var ctx1 = document.getElementById("canvas").getContext("2d");
		window.myPie = new Chart(ctx1).Pie(pieData);
		window.myBar = new Chart(ctx).Bar(barChartData, {
			responsive : false,
			//网格线的颜色
       		scaleGridLineColor : "rgba(0,0,0,.2)"

		});
	};

