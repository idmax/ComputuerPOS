
//商品管理
//获取所有商品
function getAlltype(){
	var data = "";
	var tr; //行数据
	var $table = $("table");
	typeAPI.getAllGoodsTypeAPI(data,
		function(json){
			//清除原数据
			$("table tr").each(function(i,val){
				if (i != 0) {
					val.remove()
				};
			});
			$.each(json,function(i,val){
				tr ='<tr><td>1</td><td class="none_id">'+
					val.typeId+'</td><td>'+
					val.typeName+'</td><td>'+
					val.desription+'</td><td><button class="alter">修改</button><button class="detect">删除</button></td></tr>'
				// alert(tr);
				$table.append(tr);
			})
			trNum();
		},
		function(){

		})
}


// getAllGoodsType();
//添加商品
$(".add-btn").click(function(){
	$(".addGoods_page").css("display","block");
	// getAlltype();

});
//关闭弹出窗口
$(".close_icon").click(function(){
	$(".addGoods_page").css("display","none");
	$(".updateGoods_page").css("display","none");
})

$("#addGoods").click(function(){
	var typeName = $("#add_typeName").val();
	var typeDisc = $("#add_typeDisc").val();
	// alert(goodsType);
	if (typeName == "" || typeDisc == "" ) {
		alert("您填写的信息不完整，请检查填写~");
	}
	else{
		var data = {
		"type.typeName":typeName,
		"type.desription":typeDisc
		}
		typeAPI.addTypeAPI(data,
			function(){
				alert("已成功添加商品");
				$(".addGoods_page").css("display","none");
				getAlltype();
			},
			function(){
				alert("添加失败，请重新添加");
			})
	}
	
});


//修改商品
var temp_typeId = "";
$("table").on('click','tr td .alter',function() {
    $(".updateGoods_page").css("display","block");
    var typeId = $(this).parent().parent().find("td").eq(1).text();
    var typeName = $(this).parent().parent().find("td").eq(2).text();
    var typeDisc = $(this).parent().parent().find("td").eq(3).text();

    $("#update_typeName").val(typeName);
    $("#update_typeDisc").val(typeDisc);
    temp_typeId = typeId;
});
$("#updateGoods").click(function(){

	var typeId = temp_typeId;
    var typeName = $("#update_typeName").val();
    var typeDisc = $("#update_typeDisc").val();

	if (typeName == "" || typeDisc == "" ) {
		alert("您填写的信息不完整，请检查填写~");
	}
	else{
	    var data ={
	    	"type.typeId":parseInt(typeId),
	    	"type.typeName":typeName,
	    	"type.desription":typeDisc
	    }
	    typeAPI.updateTypeAPI(data,
	    	function(){
	    		alert("修改成功");
	    		$(".updateGoods_page").css("display","none");
	    		getAlltype();
	    	},
	    	function(){
	    		alert("修改失败");
	    	})
	}
})

//删除类型
$("table").on('click',"tr td .detect",function(){
	var typeId =parseInt($(this).parent().parent().find("td").eq(1).text()) ;
	var data = {
		"type.typeId":typeId
	}
	typeAPI.deleteTypeAPI(data,
		function(){
			alert("删除成功");
			getAlltype();
		},
		function(){
			alert("删除失败");
		})
})

//搜索商品
