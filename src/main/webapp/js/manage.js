
//商品管理
//获取所有商品
function getAllgoods(){
	var data = "";
	var tr; //行数据
	var $table = $("table");
	goodsAPI.getAllGoodsAPI(data,
		function(json){
			//清除原数据
			$("table tr").each(function(i,val){
				if (i != 0) {
					val.remove()
				};
			});
			$.each(json,function(i,val){
				tr ='<tr><td>1</td><td class="none_id">'+
					val.goodsId+'</td><td>'+
					val.goodsNumber+'</td><td>'+
					val.goodsName+'</td><td>'+
					val.price.toFixed(2)+'</td><td>'+
					val.type.typeName+'</td><td>'+
					val.discount.toFixed(2)+'</td><td><button class="alter">修改</button><button class="detect">删除</button></td></tr>';
				// alert(tr);
				$table.append(tr);
			})
			trNum();
		},
		function(){

		})
}

//获取所有的类型
function getAllGoodsType(){
	var data = "";
	var option; //选项
	var $select = $(".type_option");
	typeAPI.getAllGoodsTypeAPI(data,
		function(json){
			//清除原数据
			$select.empty();
			$.each(json,function(i,val){
				option = '<option value="'+val.typeId+'">'+val.typeName+'</option>';
				// alert(tr);
				$select.append(option);
			})
		},
		function(){

		})
}
// getAllGoodsType();
//添加商品
$(".add-btn").click(function(){
	$(".addGoods_page").css("display","block");
	getAllGoodsType();

});
//关闭弹出窗口
$(".close_icon").click(function(){
	$(".addGoods_page").css("display","none");
	$(".updateGoods_page").css("display","none");
})

$("#addGoods").click(function(){
		var goodsName = $("#add_goodsName").val();
	var goodsPrice = $("#add_goodsPrice").val();
	var goodsType = $(".type_option").val();
	var goodsNum = parseInt($("table tr:last-child").find("td").eq(2).text()) + 1;
	// alert(goodsType);
	if (goodsName == "" || goodsPrice == "" || goodsType == "" || goodsNum == NaN || goodsNum == undefined ) {
		alert("您填写的信息不完整，请检查填写~");
	}
	else{
		var data = {
		"goods.goodsNumber":goodsNum,
		"goods.GoodsName":encodeURI(goodsName),
		"goods.Price":parseFloat(goodsPrice),
		"goods.type.typeId":parseInt(goodsType)
		}
		goodsAPI.addGoodsAPI(data,
			function(){
				alert("已成功添加商品");
				$(".addGoods_page").css("display","none");
				getAllgoods();
			},
			function(){
				alert("添加失败，请重新添加");
			})
	}
	
});


//修改商品
var temp_goodsId = "";
$("table").on('click','tr td .alter',function() {
	getAllGoodsType();
    $(".updateGoods_page").css("display","block");
    var goodsId = $(this).parent().parent().find("td").eq(1).text();
    var goodsNum = $(this).parent().parent().find("td").eq(2).text();
    var goodsName = $(this).parent().parent().find("td").eq(3).text();
    var goodsPrice = $(this).parent().parent().find("td").eq(4).text();
    var goodsDisc = $(this).parent().parent().find("td").eq(6).text();
    var goodsType = $(".updateGoods_page .type_option").val();

    $("#update_goodsName").val(goodsName);
    $("#update_goodsPrice").val(goodsPrice);
    $("#update_goodsDisc").val(goodsDisc);
    temp_goodsId = goodsId;
});
$("#updateGoods").click(function(){

	var goodsId = temp_goodsId;
    var goodsName = $("#update_goodsName").val();
    var goodsPrice = $("#update_goodsPrice").val();
    var goodsDisc = $("#update_goodsDisc").val();
    var goodsType = $(".updateGoods_page .type_option").val();

	if (goodsName == "" || goodsPrice == "" || goodsType == "" || goodsDisc == "" ) {
		alert("您填写的信息不完整，请检查填写~");
	}
	else{
	    var data ={
	    	"goods.goodsId":parseInt(goodsId),
	    	"goods.GoodsName":goodsName,
	    	"goods.price":parseFloat(goodsPrice),
	    	"goods.type.typeId":parseInt(goodsType),
	    	"goods.discount":parseFloat(goodsDisc)
	    }
	    goodsAPI.updateGoodsAPI(data,
	    	function(){
	    		alert("修改成功");
	    		$(".updateGoods_page").css("display","none");
	    		getAllgoods();
	    	},
	    	function(){
	    		alert("修改失败");
	    	})
	}
})

//删除商品
$("table").on('click',"tr td .detect",function(){
	var goodsNum = $(this).parent().parent().find("td").eq(2).text();
	var data = {
		"goods.goodsNumber":goodsNum
	}
	goodsAPI.deleteGoodsAPI(data,
		function(){
			alert("删除成功");
			getAllgoods();
		},
		function(){
			alert("删除失败");
		})
})

//搜索商品
