package vo;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Salereturn entity. @author MyEclipse Persistence Tools
 */

public class Salereturn implements java.io.Serializable {

	// Fields

	private Integer id;
	private Order order;
	private String salereturnNo;
	private String reason;
	private Double total;
	private Timestamp createTime;
	private Set salereturnitems = new HashSet(0);

	// Constructors

	/** default constructor */
	public Salereturn() {
	}

	/** minimal constructor */
	public Salereturn(Order order, String salereturnNo, String reason,
			Double total, Timestamp createTime) {
		this.order = order;
		this.salereturnNo = salereturnNo;
		this.reason = reason;
		this.total = total;
		this.createTime = createTime;
	}

	/** full constructor */
	public Salereturn(Order order, String salereturnNo, String reason,
			Double total, Timestamp createTime, Set salereturnitems) {
		this.order = order;
		this.salereturnNo = salereturnNo;
		this.reason = reason;
		this.total = total;
		this.createTime = createTime;
		this.salereturnitems = salereturnitems;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Order getOrder() {
		return this.order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public String getSalereturnNo() {
		return this.salereturnNo;
	}

	public void setSalereturnNo(String salereturnNo) {
		this.salereturnNo = salereturnNo;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Double getTotal() {
		return this.total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Set getSalereturnitems() {
		return this.salereturnitems;
	}

	public void setSalereturnitems(Set salereturnitems) {
		this.salereturnitems = salereturnitems;
	}

}