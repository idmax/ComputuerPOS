package vo;

import org.apache.struts2.json.annotations.JSON;

/**
 * Orderitem entity. @author MyEclipse Persistence Tools
 */

public class Orderitem implements java.io.Serializable {

	// Fields

	private Integer orderItemId;
	private Goods goods;
	private Order order;
	private Double price;
	private Integer saleNumber;
	private Double discount;
	private Integer state;
	// Constructors

	/** default constructor */
	public Orderitem() {
	}

	/** full constructor */
	public Orderitem(Goods goods, Order order, Double price,
			Integer saleNumber, Double discount, Integer state) {
		this.goods = goods;
		this.order = order;
		this.price = price;
		this.saleNumber = saleNumber;
		this.discount = discount;
		this.state = state;
	}

	// Property accessors

	public Integer getOrderItemId() {
		return this.orderItemId;
	}

	public void setOrderItemId(Integer orderItemId) {
		this.orderItemId = orderItemId;
	}
	public Goods getGoods() {
		return this.goods;
	}

	public void setGoods(Goods goods) {
		this.goods = goods;
	}
	@JSON(serialize=false)
	public Order getOrder() {
		return this.order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getSaleNumber() {
		return this.saleNumber;
	}

	public void setSaleNumber(Integer saleNumber) {
		this.saleNumber = saleNumber;
	}

	public Double getDiscount() {
		return this.discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public int getGoodsId(){
		return this.goods.getGoodsNumber();
	}
	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}
}