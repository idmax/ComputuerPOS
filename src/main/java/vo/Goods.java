package vo;

import java.util.HashSet;
import java.util.Set;

import org.apache.struts2.json.annotations.JSON;

/**
 * Goods entity. @author MyEclipse Persistence Tools
 */

public class Goods implements java.io.Serializable {

	// Fields

	private Integer goodsId;
	private Type type;
	private Integer goodsNumber;
	private String goodsName;
	private Double price;
	private Double discount;
	private Set orderitems = new HashSet(0);

	// Constructors

	/** default constructor */
	public Goods() {
	}

	/** minimal constructor */
	public Goods(Integer goodsNumber, String goodsName, Double price,
			Double discount) {
		this.goodsNumber = goodsNumber;
		this.goodsName = goodsName;
		this.price = price;
		this.discount = discount;
	}

	/** full constructor */
	public Goods(Type type, Integer goodsNumber, String goodsName,
			Double price, Double discount, Set orderitems) {
		this.type = type;
		this.goodsNumber = goodsNumber;
		this.goodsName = goodsName;
		this.price = price;
		this.discount = discount;
		this.orderitems = orderitems;
	}

	// Property accessors

	public Integer getGoodsId() {
		return this.goodsId;
	}

	public void setGoodsId(Integer goodsId) {
		this.goodsId = goodsId;
	}
	public Type getType() {
		return this.type;
	}

	public void setType(Type type) {
		this.type = type;
	}
	public Integer getGoodsNumber() {
		return this.goodsNumber;
	}

	public void setGoodsNumber(Integer goodsNumber) {
		this.goodsNumber = goodsNumber;
	}

	public String getGoodsName() {
		return this.goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getDiscount() {
		return this.discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	@JSON(serialize=false)
	public Set getOrderitems() {
		return this.orderitems;
	}

	public void setOrderitems(Set orderitems) {
		this.orderitems = orderitems;
	}

}