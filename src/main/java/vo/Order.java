package vo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import vo.Orderitem;

import org.apache.struts2.json.annotations.JSON;

/**
 * Order entity. @author MyEclipse Persistence Tools
 */

public class Order implements java.io.Serializable {

	// Fields

	private Integer orderId;
	private Users users;
	private String userNumber;
	private String orderNumber;
	private Timestamp orderTime;
	private Double totalPrice;
	private Integer isPayment;
	private Set orderitems = new HashSet(0);
	private ArrayList orderitemsId;
	private Set payments = new HashSet(0);
	private String paymentNumber;

	// Constructors

	/** default constructor */
	public Order() {
	}

	/** minimal constructor */
	public Order(Users users, String orderNumber, Timestamp orderTime,
			Double totalPrice, Integer isPayment) {
		this.users = users;
		this.orderNumber = orderNumber;
		this.orderTime = orderTime;
		this.totalPrice = totalPrice;
		this.isPayment = isPayment;
	}

	/** full constructor */
	public Order(Users users, String orderNumber, Timestamp orderTime,
			Double totalPrice, Integer isPayment, Set orderitems, Set payments) {
		this.users = users;
		this.orderNumber = orderNumber;
		this.orderTime = orderTime;
		this.totalPrice = totalPrice;
		this.isPayment = isPayment;
		this.orderitems = orderitems;
		this.payments = payments;
	}

	// Property accessors

	public Integer getOrderId() {
		return this.orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	@JSON(serialize=false)
	public Users getUsers() {
		return this.users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	public String getOrderNumber() {
		return this.orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Timestamp getOrderTime() {
		return this.orderTime;
	}

	public void setOrderTime(Timestamp orderTime) {
		this.orderTime = orderTime;
	}

	public Double getTotalPrice() {
		return this.totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Integer getIsPayment() {
		return this.isPayment;
	}

	public void setIsPayment(Integer isPayment) {
		this.isPayment = isPayment;
	}
@JSON(serialize=false)
	public Set getOrderitems() {
		return this.orderitems;
	}
	
	public void setOrderitems(Set orderitems) {
		this.orderitems = orderitems;
	}
@JSON(serialize=false)
	public Set getPayments() {
		return this.payments;
	}

	public void setPayments(Set payments) {
		this.payments = payments;
	}

}