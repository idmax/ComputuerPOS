package vo;

import java.sql.Timestamp;

import org.apache.struts2.json.annotations.JSON;

/**
 * Payment entity. @author MyEclipse Persistence Tools
 */

public class Payment implements java.io.Serializable {

	// Fields

	private Integer paymentId;
	private Users users;
	private String userNumber;
	private Order order;
	private String orderNumber;
	private String paymentNo;
	private Timestamp createTime;

	// Constructors

	/** default constructor */
	public Payment() {
	}

	/** full constructor */
	public Payment(Users users, Order order, String paymentNo,
			Timestamp createTime) {
		this.users = users;
		this.order = order;
		this.paymentNo = paymentNo;
		this.createTime = createTime;
	}

	// Property accessors

	public Integer getPaymentId() {
		return this.paymentId;
	}

	public void setPaymentId(Integer paymentId) {
		this.paymentId = paymentId;
	}
	@JSON(serialize=false)
	public Users getUsers() {
		return this.users;
	}
	public void setUsers(Users users) {
		this.users = users;
	}
	public String getUserNumber(){
		return this.userNumber;
	}
	public void setUserNumber(){
		this.userNumber=this.users.getUserNumber();
	}
	@JSON(serialize=false)
	public Order getOrder() {
		return this.order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public String getOrderNumber(){
		return this.orderNumber;
	}
	public void setOrderNumber(){
		this.orderNumber=this.order.getOrderNumber();
	}
	public String getPaymentNo() {
		return this.paymentNo;
	}

	public void setPaymentNo(String paymentNo) {
		this.paymentNo = paymentNo;
	}

	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

}