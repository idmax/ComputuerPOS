package vo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.struts2.json.annotations.JSON;

/**
 * Users entity. @author MyEclipse Persistence Tools
 */

public class Users implements java.io.Serializable {

	// Fields

	private Integer userId;
	private String userNumber;
	private String userName;
	private String userPasswd;
	private String gender;
	private String tel;
	private Set orders = new HashSet(0);
	private Set payments = new HashSet(0);

	// Constructors

	/** default constructor */
	public Users() {
	}

	/** minimal constructor */
	public Users(String userNumber, String userName,
			String userPasswd, String gender) {
		this.userNumber = userNumber;
		this.userName = userName;
		this.userPasswd = userPasswd;
		this.gender = gender;
	}

	/** full constructor */
	public Users(String userNumber, String userName,
			String userPasswd, String gender, String tel, Set orders,
			Set payments) {
		this.userNumber = userNumber;
		this.userName = userName;
		this.userPasswd = userPasswd;
		this.gender = gender;
		this.tel = tel;
		this.orders = orders;
		this.payments = payments;
	}

	// Property accessors

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserNumber() {
		return this.userNumber;
	}

	public void setUserNumber(String userNumber) {
		this.userNumber = userNumber;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPasswd() {
		return this.userPasswd;
	}

	public void setUserPasswd(String userPasswd) {
		this.userPasswd = userPasswd;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getTel() {
		return this.tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}
	@JSON(serialize=false)
	public Set getOrders() {
		return this.orders;
	}
//	public ArrayList getOrderId(){
//		
//		return this.orderId;
//	}
//	public void setOrderId(){
//		Iterator it=orders.iterator();
//		while(it.hasNext()){
//			orderId.add(((Order)it.next()).getOrderId());
//		}
//	}
	public void setOrders(Set orders) {
		this.orders = orders;
	}
	@JSON(serialize=false)
	public Set getPayments() {
		return this.payments;
	}

	public void setPayments(Set payments) {
		this.payments = payments;
	}

}