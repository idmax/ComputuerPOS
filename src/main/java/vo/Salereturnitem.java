package vo;

/**
 * Salereturnitem entity. @author MyEclipse Persistence Tools
 */

public class Salereturnitem implements java.io.Serializable {

	// Fields

	private Integer id;
	private Goods goods;
	private Salereturn salereturn;
	private Integer quantity;
	private Double price;

	// Constructors

	/** default constructor */
	public Salereturnitem() {
	}

	/** full constructor */
	public Salereturnitem(Goods goods, Salereturn salereturn, Integer quantity,
			Double price) {
		this.goods = goods;
		this.salereturn = salereturn;
		this.quantity = quantity;
		this.price = price;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Goods getGoods() {
		return this.goods;
	}

	public void setGoods(Goods goods) {
		this.goods = goods;
	}

	public Salereturn getSalereturn() {
		return this.salereturn;
	}

	public void setSalereturn(Salereturn salereturn) {
		this.salereturn = salereturn;
	}

	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

}