package service;
import vo.Order;
import java.sql.Timestamp;
import java.util.List;
public interface IOrderService {
	public Order creatOrder(Order o);
	public List getOrderByNumber(String orderNumber);
	public List getAllOrder();
	public List getOrderByTime(Timestamp start,Timestamp end);
	public List getOrderByUserName(String userName);
}
