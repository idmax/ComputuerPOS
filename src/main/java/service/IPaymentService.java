package service;
import vo.Payment;
public interface IPaymentService {
	public Payment addPayment(Payment payment);
	public Payment getPaymentByNo(String paymentNo);
}
