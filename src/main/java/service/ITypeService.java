package service;
import java.util.List;
import vo.Type;
public interface ITypeService {
	public List getAllType();
	public void update(Type type);
	public void addType(Type type);
	public void deleteTypeByTypeId(int id);
	public List getTypeById(int id);
	public List getTypeByTypeName(String typeName);
}
