package service;
import vo.Orderitem;
import java.sql.Timestamp;
import java.util.List;
public interface IOrderItemService {
	public Orderitem createOrderItem(Orderitem orderItem);
	public List getAllOrderItem();
	public List getOrderItemByOrderId(int orderId);
	public List getOrderItemByTime(Timestamp start,Timestamp end);
}
