package service;
import vo.Users;
import vo.UserRole;
import java.util.List;
public interface IUserService {
	public List getUserById(int id);
	public List getUserByUserNumber(String userNumber);
	public void deleteUser(String userNumber);
	public void addUser(Users user,UserRole userRole);
	public List getAllUser();
	public void updateUser(Users user);
	public List getUserByName(String name);
}
