package service;
import vo.Permission;
import vo.UserRole;
import vo.RolePermission;
import vo.UserRole;
import vo.Role;
import java.util.List;
public interface IPermissionService {
	public void addPer(Permission per);
	public void deletePer(int id);
	public List getPer(int id);
	public List getAllPer();
	public void updatePermission(Permission per);
	
	public void addRole(Role role);
	public void deleteRole(int id);
	public List getRoleById(int id);
	public List getAllRole();
	public void updateRole(Role role);
	
	
	public void addRolePer(RolePermission rp);
	public void deleteRolePer(int id);
	public List getRolePerByRoleId(int roleId);
	public void addUserRole(UserRole userRole);
	public void deleteUserRole(int id);
	public List getUserRoleByUserId(int userId);
}
