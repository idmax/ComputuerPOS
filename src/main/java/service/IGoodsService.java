package service;
import vo.Goods;
import java.util.List;
public interface IGoodsService {

	public List getAllGoods();
	public List getGoodsByGoodsNumber(int goodsNumber);
	public void deleteGoods(int goodsNumber);
	public void addGoods(Goods goods);
	public void update(Goods goods);
	//public List getGoodsByMultiCondition(Goods goods);
	public List getGoodsByGoodsName(String goodsName);
}
