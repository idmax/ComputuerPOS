package service.impl;

import vo.Salereturnitem;
import dao.ISaleReturnItemDao;
import service.ISaleReturnItemService;
import dao.IOrderItemDao;
public class SaleReturnItemService implements ISaleReturnItemService{
	private IOrderItemDao orderItemDao;
	private ISaleReturnItemDao saleReturnItemDao;
	public void addSaleReturnItem(Salereturnitem saleReturnItem,int orderItemId) {
		this.saleReturnItemDao.addSaleReturnItem(saleReturnItem);
		this.orderItemDao.updateOrderItemState(orderItemId);

	}
	public ISaleReturnItemDao getSaleReturnItemDao() {
		return saleReturnItemDao;
	}
	public void setSaleReturnItemDao(ISaleReturnItemDao saleReturnItemDao) {
		this.saleReturnItemDao = saleReturnItemDao;
	}
	public IOrderItemDao getOrderItemDao() {
		return orderItemDao;
	}
	public void setOrderItemDao(IOrderItemDao orderItemDao) {
		this.orderItemDao = orderItemDao;
	}
	
}
