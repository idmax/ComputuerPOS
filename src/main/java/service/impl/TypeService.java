package service.impl;

import java.util.List;

import service.ITypeService;
import vo.Type;
import dao.ITypeDao;
public class TypeService implements ITypeService {

	private ITypeDao typeDao;
	public List getAllType() {
		return typeDao.getAllType();
	}
	public void update(Type type){
		this.typeDao.update(type);
	}
	public void addType(Type type){
		this.typeDao.addType(type);
	}
	public void deleteTypeByTypeId(int id){
		this.typeDao.deleteType(id);
	}
	public List getTypeById(int id){
		return this.typeDao.getTypeById(id);
	}
	public List getTypeByTypeName(String typeName){
		return this.typeDao.getTypeByTypeName(typeName);
	}
	
	
	public ITypeDao getTypeDao() {
		return typeDao;
	}
	public void setTypeDao(ITypeDao typeDao) {
		this.typeDao = typeDao;
	}

}
