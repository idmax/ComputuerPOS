package service.impl;

import java.util.List;

import service.IPermissionService;
import vo.Permission;
import vo.Role;
import vo.RolePermission;
import vo.UserRole;
import dao.IRoleDao;
import dao.IPermissionDao;
import dao.IRolePermissionDao;
import dao.IUserRoleDao;
public class PermissionService implements IPermissionService {
	private IPermissionDao permissionDao;
	private IRoleDao roleDao;
	private IRolePermissionDao rolePermissionDao;
	private IUserRoleDao userRoleDao;
	@Override
	public void addPer(Permission per) {
		this.permissionDao.addPer(per);

	}

	@Override
	public void deletePer(int id) {
		this.permissionDao.deletePer(id);

	}
	public List getAllPer(){
		return this.permissionDao.getAllPer();
	}
	@Override
	public List getPer(int id) {
		return this.permissionDao.getPer(id);
	}
	public void updatePermission(Permission per){
		this.permissionDao.update(per);
	}
	
	
	public void addRole(Role role){
		this.roleDao.addRole(role);
	}
	public void deleteRole(int id){
		this.roleDao.deleteRole(id);
	}
	public List getRoleById(int id){
		return this.roleDao.getRoleById(id);
	}
	public void updateRole(Role role){
		this.roleDao.update(role);
	}
	
	
	public List getAllRole(){
		return this.roleDao.getAllRole();
	}
	
	public void addRolePer(RolePermission rolePer){
		this.rolePermissionDao.add(rolePer);
	}
	public void deleteRolePer(int id){
		this.rolePermissionDao.delete(id);
	}
	public List getRolePerByRoleId(int roleId){
		return this.rolePermissionDao.getByRoleId(roleId);
	}
	
	
	public void addUserRole(UserRole userRole){
		this.userRoleDao.addUserRole(userRole);
	}
	public void deleteUserRole(int id){
		this.userRoleDao.deleteUserRole(id);
	}
	public List getUserRoleByUserId(int userId){
		return this.userRoleDao.getUserRoleByUserId(userId);
	}
	
	
	
	public IPermissionDao getPermissionDao() {
		return permissionDao;
	}

	public void setPermissionDao(IPermissionDao permissionDao) {
		this.permissionDao = permissionDao;
	}

	public IRoleDao getRoleDao() {
		return roleDao;
	}

	public void setRoleDao(IRoleDao roleDao) {
		this.roleDao = roleDao;
	}

	public IRolePermissionDao getRolePermissionDao() {
		return rolePermissionDao;
	}

	public void setRolePermissionDao(IRolePermissionDao rolePermissionDao) {
		this.rolePermissionDao = rolePermissionDao;
	}

	public IUserRoleDao getUserRoleDao() {
		return userRoleDao;
	}

	public void setUserRoleDao(IUserRoleDao userRoleDao) {
		this.userRoleDao = userRoleDao;
	}
	
}
