package service.impl;

import java.util.List;
import vo.Goods;
import service.IGoodsService;
import dao.IGoodsDao;
public class GoodsService implements IGoodsService {
	
	private IGoodsDao goodsDao;
	@Override
	public List getAllGoods() {
		return goodsDao.allGoods();
		
	}
	public List getGoodsByGoodsNumber(int goodsNumber){
		return goodsDao.getGoodsByGoodsNumber(goodsNumber);
	}
	public void update(Goods goods){
		this.goodsDao.update(goods);
	}
//	public List getGoodsByMultiCondition(Goods goods){
//		return this.goodsDao.getGoodsByMultiCondition(goods);
//	}
	public List getGoodsByGoodsName(String goodsName){
		return this.goodsDao.getGoodsByGoodsName(goodsName);
	}
	
	public void deleteGoods(int goodsNumber){
		goodsDao.deleteGoods(goodsNumber);
	}
	public void addGoods(Goods goods){
		goods.setDiscount(1.0);
		goodsDao.addGoods(goods);
	}
	public IGoodsDao getGoodsDao() {
		return goodsDao;
	}
	public void setGoodsDao(IGoodsDao goodsDao) {
		this.goodsDao = goodsDao;
	}
	
}
