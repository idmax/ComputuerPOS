package service.impl;

import service.IOrderItemService;
import java.sql.Timestamp;
import java.util.List;
import dao.IOrderItemDao;
import dao.IGoodsDao;
import dao.IOrderDao;
import vo.Orderitem;
import vo.Goods;
public class OrderItemService implements IOrderItemService {
	private IOrderItemDao orderItemDao;
	private IOrderDao orderDao;
	private IGoodsDao goodsDao;
	@Override
	public Orderitem createOrderItem(Orderitem orderItem) {
		Goods goods=goodsDao.getGoodsById(orderItem.getGoods().getGoodsId());
		orderItem.setSaleNumber(1);
		orderItem.setDiscount(goods.getDiscount());
		orderItem.setPrice(goods.getPrice());
		Orderitem oi=orderItemDao.addOrderItem(orderItem);
		return oi;
	}
	public List getAllOrderItem(){
		return this.orderItemDao.getAllOrderItem();
	}
	public List getOrderItemByOrderId(int orderId){
		return this.orderItemDao.getOrderItemByOrderId(orderId);
	}
	public List getOrderItemByTime(Timestamp start,Timestamp end){
		return this.orderItemDao.getOrderItemByTime(start, end);
	}
	
	public IOrderItemDao getOrderItemDao() {
		return orderItemDao;
	}
	public void setOrderItemDao(IOrderItemDao orderItemDao) {
		this.orderItemDao = orderItemDao;
	}
	public IOrderDao getOrderDao() {
		return orderDao;
	}
	public void setOrderDao(IOrderDao orderDao) {
		this.orderDao = orderDao;
	}
	public IGoodsDao getGoodsDao() {
		return goodsDao;
	}
	public void setGoodsDao(IGoodsDao goodsDao) {
		this.goodsDao = goodsDao;
	}
	
	

}
