package service.impl;

import service.IUserService;
import dao.IUserRoleDao;
import vo.UserRole;
import java.util.List;
import vo.Users;
import dao.IUserDao;
import dao.IRoleDao;
public class UserService implements IUserService {
	private IUserDao userDao;
	private IUserRoleDao userRoleDao;
	@Override
	public List getUserById(int id) {
		return userDao.getUserById(id);
		
	}
	public void addUser(Users user,UserRole userRole){
		user.setUserPasswd("1234");
		user=userDao.addUser(user);
		userRole.setUserId(user.getUserId());
		userRoleDao.addUserRole(userRole);
	}
	public List getAllUser(){
		return userDao.getAllUsers();
	}
	public void updateUser(Users user){
		this.userDao.update(user);
	}
	public List getUserByName(String name){
		return this.userDao.getUserByName(name);
	}
	
	
	
	public IUserDao getUserDao() {
		return userDao;
	}
	
	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}
	public List getUserByUserNumber(String userNumber){
		return this.userDao.getUserByUserNumber(userNumber);
	}
	public void deleteUser(String userNumber){
		this.userDao.deleteUser(userNumber);
	}
	public IUserRoleDao getUserRoleDao() {
		return userRoleDao;
	}
	public void setUserRoleDao(IUserRoleDao userRoleDao) {
		this.userRoleDao = userRoleDao;
	}
	
}
