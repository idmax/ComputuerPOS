package service.impl;
import java.sql.Timestamp;

import service.ISaleReturnService;
import dao.ISaleReturnDao;
import vo.Salereturn;
public class SaleReturnService implements ISaleReturnService{
	private ISaleReturnDao saleReturnDao;
	public void createSaleReturn(Salereturn sr){
		sr.setCreateTime(new Timestamp(System.currentTimeMillis()));
		this.saleReturnDao.addSaleReturn(sr);
	}
	public ISaleReturnDao getSaleReturnDao() {
		return saleReturnDao;
	}
	public void setSaleReturnDao(ISaleReturnDao saleReturnDao) {
		this.saleReturnDao = saleReturnDao;
	}
	
}
