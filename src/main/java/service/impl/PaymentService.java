package service.impl;

import java.sql.Timestamp;

import service.IPaymentService;
import vo.Payment;
import vo.Order;
import dao.IPaymentDao;
import dao.IOrderDao;
public class PaymentService implements IPaymentService {
	private IPaymentDao paymentDao;
	private IOrderDao orderDao;
	@Override
	public Payment addPayment(Payment payment) {
		payment.setCreateTime(new Timestamp(System.currentTimeMillis()));
		paymentDao.addPayment(payment);
		Order o=orderDao.getOrderById(payment.getOrder().getOrderId());
		o.setIsPayment(1);
		orderDao.setOrder(o);;
		return paymentDao.getPaymentByNo(payment.getPaymentNo());
	}

	@Override
	public Payment getPaymentByNo(String paymentNo) {
		return paymentDao.getPaymentByNo(paymentNo);
	}

	public IPaymentDao getPaymentDao() {
		return paymentDao;
	}

	public void setPaymentDao(IPaymentDao paymentDao) {
		this.paymentDao = paymentDao;
	}

	public IOrderDao getOrderDao() {
		return orderDao;
	}

	public void setOrderDao(IOrderDao orderDao) {
		this.orderDao = orderDao;
	}

}
