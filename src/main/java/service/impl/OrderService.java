package service.impl;

import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import vo.Order;
import vo.Users;
import dao.IOrderDao;
import service.IOrderService;
import service.IOrderItemService;
import service.IUserService;
public class OrderService implements IOrderService {
	private IOrderDao orderDao;
	private IUserService userService;
	@Override
	public Order creatOrder(Order o) {
		o.setOrderTime(new Timestamp(System.currentTimeMillis()));
		o.setIsPayment(0);
		return orderDao.addOrder(o);
	}
	public List getAllOrder(){
		return this.orderDao.getAllOrder();
	}
	public List getOrderByNumber(String orderNumber){
		return orderDao.getOrderByNumber(orderNumber);
	}
	public List getOrderByTime(Timestamp start,Timestamp end){
		return this.orderDao.getOrderByTime(start, end);
	}
	public List getOrderByUserName(String userName){
		return this.orderDao.getOrderByUserName(userName);
	}
	
	
	public IOrderDao getOrderDao() {
		return orderDao;
	}
	public void setOrderDao(IOrderDao orderDao) {
		this.orderDao = orderDao;
	}
	public IUserService getUserService() {
		return userService;
	}
	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

}
