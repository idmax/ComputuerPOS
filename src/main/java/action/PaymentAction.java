package action;
import com.opensymphony.xwork2.ActionSupport;
import vo.Payment;
import service.IPaymentService;
public class PaymentAction extends ActionSupport{
	private IPaymentService paymentService;
	private Payment payment;
	public String createPayment(){
		payment=paymentService.addPayment(payment);
		return SUCCESS;
	}
	
	
	
	
	public IPaymentService getPaymentService() {
		return paymentService;
	}
	public void setPaymentService(IPaymentService paymentService) {
		this.paymentService = paymentService;
	}
	public Payment getPayment() {
		return payment;
	}
	public void setPayment(Payment payment) {
		this.payment = payment;
	}
}
