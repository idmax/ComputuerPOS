package action;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.Timestamp;
import vo.Order;
import java.util.List;
import vo.Orderitem;
import service.IOrderItemService;
public class OrderItemAction extends ActionSupport{
	private Orderitem orderItem;
	private List orderItems;
	private Order order;
	private Timestamp start,end;
	private IOrderItemService orderItemService;
	public String createOrderItem(){
		orderItem=orderItemService.createOrderItem(orderItem);
		return SUCCESS;
	}
	public String getAllOrderItem(){
		orderItems=this.orderItemService.getAllOrderItem();
		return SUCCESS;
	}
	public String getOrderItemByOrderId(){
		orderItems=this.orderItemService.getOrderItemByOrderId(order.getOrderId());
		return SUCCESS;
	}
	public String getOrderItemByTime(){
		orderItems=this.orderItemService.getOrderItemByTime(start, end);
		return SUCCESS;
	}
	
	
	public Orderitem getOrderItem() {
		return orderItem;
	}
	public void setOrderItem(Orderitem orderItem) {
		this.orderItem = orderItem;
	}
	public IOrderItemService getOrderItemService() {
		return orderItemService;
	}
	public void setOrderItemService(IOrderItemService orderItemService) {
		this.orderItemService = orderItemService;
	}
	public List getOrderItems() {
		return orderItems;
	}
	public void setOrderItems(List orderItems) {
		this.orderItems = orderItems;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public Timestamp getStart() {
		return start;
	}
	public void setStart(Timestamp start) {
		this.start = start;
	}
	public Timestamp getEnd() {
		return end;
	}
	public void setEnd(Timestamp end) {
		this.end = end;
	}
	
}
