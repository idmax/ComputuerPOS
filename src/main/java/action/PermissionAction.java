package action;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import service.IPermissionService;
import vo.Permission;
import vo.Role;
import vo.UserRole;
import vo.RolePermission;
public class PermissionAction extends ActionSupport{
	private Permission per;
	private IPermissionService permissionService;
	private Role role;
	private RolePermission rolePer;
	private UserRole userRole;
	private List pers;
	private List roles;
	private List rolePers;
	private List userRoles;
	public String addPer(){
		permissionService.addPer(per);
		return SUCCESS;
	}
	public String deletePer(){
		permissionService.deletePer(this.per.getId());
		return SUCCESS;
	}
	public String getPerById(){
		pers=permissionService.getPer(this.per.getId());
		return SUCCESS;
	}
	public String getAllPer(){
		pers=this.permissionService.getAllPer();
		return SUCCESS;
	}
	public String updatePermission(){
		this.permissionService.updatePermission(per);
		return SUCCESS;
	}
	
	public String addRole(){
		this.permissionService.addRole(role);
		return SUCCESS;
	}
	public String getRoleById(){
		roles=this.permissionService.getRoleById(role.getRoleId());
		
		return SUCCESS;
	}
	public String deleteRole(){
		this.permissionService.deleteRole(role.getRoleId());
		return SUCCESS;
	}
	public String getAllRole(){
		roles=this.permissionService.getAllRole();
		return SUCCESS;
	}
	public String updateRole(){
		this.permissionService.updateRole(role);
		return SUCCESS;
	}
	
	
	public String addRolePer(){
		this.permissionService.addRolePer(rolePer);
		return SUCCESS;
	}
	public String deleteRolePer(){
		this.permissionService.deleteRolePer(this.rolePer.getId());
		return SUCCESS;
	}
	public String getRolePerByRoleId(){
		rolePers=this.permissionService.getRolePerByRoleId(this.rolePer.getRoleId());
		return SUCCESS;
	}
	
	
	public String addUserRole(){
		this.permissionService.addUserRole(userRole);
		return SUCCESS;
	}
	public String deleteUserRole(){
		this.permissionService.deleteUserRole(this.userRole.getId());
		return SUCCESS;
	}
	public String getUserRoleByUserId(){
		userRoles=this.permissionService.getUserRoleByUserId(this.userRole.getUserId());
		return SUCCESS;
	}
	
	
	public Permission getPer() {
		return per;
	}
	public void setPer(Permission per) {
		this.per = per;
	}
	public IPermissionService getPermissionService() {
		return permissionService;
	}
	public void setPermissionService(IPermissionService permissionService) {
		this.permissionService = permissionService;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public List getPers() {
		return pers;
	}
	public void setPers(List pers) {
		this.pers = pers;
	}
	public List getRoles() {
		return roles;
	}
	public void setRoles(List roles) {
		this.roles = roles;
	}
	public RolePermission getRolePer() {
		return rolePer;
	}
	public void setRolePer(RolePermission rolePer) {
		this.rolePer = rolePer;
	}
	public List getRolePers() {
		return rolePers;
	}
	public void setRolePers(List rolePers) {
		this.rolePers = rolePers;
	}
	public UserRole getUserRole() {
		return userRole;
	}
	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}
	public List getUserRoles() {
		return userRoles;
	}
	public void setUserRoles(List userRoles) {
		this.userRoles = userRoles;
	}
	
}
