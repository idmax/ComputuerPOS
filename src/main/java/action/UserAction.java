package action;

import com.opensymphony.xwork2.ActionSupport;
import vo.UserRole;
import java.util.List;
import vo.Users;
import service.IUserService;
public class UserAction extends ActionSupport {
	private IUserService userService;
	private Users user;
	private UserRole userRole;
	private List userList;
	public String searchUser(){
		userList=this.userService.getUserByUserNumber(user.getUserNumber());
		return SUCCESS;
	}
	public String deleteUser(){
		this.userService.deleteUser(user.getUserNumber());
		return SUCCESS;
	}
	public String addUser(){
		this.userService.addUser(user,userRole);
		return SUCCESS;
	}
	public String getAllUser(){
		userList=this.userService.getAllUser();
		return SUCCESS;
	}
	public String updateUser(){
		this.userService.updateUser(user);
		return SUCCESS;
	}
	public String getUserByName(){
		userList=this.userService.getUserByName(user.getUserName());
		return SUCCESS;
	}

	public IUserService getUserService() {
		return userService;
	}
	public void setUserService(IUserService userService) {
		this.userService = userService;
	}
	public Users getUser() {
		return user;
	}
	public void setUser(Users user) {
		this.user = user;
	}
	public List getUserList() {
		return userList;
	}
	public void setUserList(List userList) {
		this.userList = userList;
	}
	public UserRole getUserRole() {
		return userRole;
	}
	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}
	
	
}
