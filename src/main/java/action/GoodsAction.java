package action;

import com.opensymphony.xwork2.ActionSupport;
import vo.Goods;
import service.impl.GoodsService;
import service.IGoodsService;
import service.ITypeService;
import java.util.List;
import vo.Type;
public class GoodsAction extends ActionSupport {
	private Goods goods;
	private Type type;
	private IGoodsService goodsService;
	private ITypeService typeService;
	private List goodsList;
	private List goodsTypeList;
	public String getAllGoods(){
		goodsList=goodsService.getAllGoods();
		return SUCCESS;
	}
	public String getAllGoodsType(){
		this.goodsTypeList=this.typeService.getAllType();
		return SUCCESS;
	}
	public String searchGoods(){
		goodsList=goodsService.getGoodsByGoodsNumber(goods.getGoodsNumber());
		return SUCCESS;
	}
	public String deleteGoods(){
		goodsService.deleteGoods(goods.getGoodsNumber());
		return SUCCESS;
	}
	public String addGoods(){
		goodsService.addGoods(goods);
		return SUCCESS;
	}
	public String updateGoods(){
		this.goodsService.update(goods);
		return SUCCESS;
	}
	public String updateType(){
		this.typeService.update(type);
		return SUCCESS;
	}
	public String addType(){
		this.typeService.addType(type);
		return SUCCESS;
	}
	public String deleteType(){
		this.typeService.deleteTypeByTypeId(this.type.getTypeId());
		return SUCCESS;
	}
	public String getTypeById(){
		goodsTypeList=this.typeService.getTypeById(type.getTypeId());
		return SUCCESS;
	}
	public String getTypeByTypeName(){
		goodsTypeList=this.typeService.getTypeByTypeName(type.getTypeName());
		return SUCCESS;
	}
//	public String getGoodsByMultiCondition(){
//		goodsList=this.goodsService.getGoodsByMultiCondition(goods);
//		return SUCCESS;
//	}
	public String getGoodsByGoodsName(){
		goodsList=this.goodsService.getGoodsByGoodsName(this.goods.getGoodsName());
		return SUCCESS;
	}
	
	public List getGoodsList() {
		return goodsList;
	}	
	public void setGoodsList(List goodsList) {
		this.goodsList = goodsList;
	}
	public Goods getGoods() {
		return goods;
	}
	public void setGoods(Goods goods) {
		this.goods = goods;
	}
	public IGoodsService getGoodsService() {
		return goodsService;
	}
	public void setGoodsService(IGoodsService goodsService) {
		this.goodsService = goodsService;
	}
	public ITypeService getTypeService() {
		return typeService;
	}
	public void setTypeService(ITypeService typeService) {
		this.typeService = typeService;
	}
	public List getGoodsTypeList() {
		return goodsTypeList;
	}
	public void setGoodsTypeList(List goodsTypeList) {
		this.goodsTypeList = goodsTypeList;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	
}
