package action;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.Timestamp;
import java.util.List;
import vo.Order;
import service.IOrderService;
public class OrderAction extends ActionSupport{
	private IOrderService orderService;
	private Order order;
	private List orderList;
	private Timestamp start,end;
	private String userName;
	public String createOrder(){
		order=orderService.creatOrder(order);
		return SUCCESS;
	}
	public String getOrderByNumber(){
		orderList=orderService.getOrderByNumber(order.getOrderNumber());
		return SUCCESS;
	}
	public String getAllOrder(){
		orderList=orderService.getAllOrder();
		return SUCCESS;
	}
	public String getOrderByTime(){
		orderList=this.orderService.getOrderByTime(start, end);
		return SUCCESS;
	}
	public String getOrderByUserName(){
		orderList=this.orderService.getOrderByUserName(userName);
		return SUCCESS;
	}
	
	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public IOrderService getOrderService() {
		return orderService;
	}
	public void setOrderService(IOrderService orderService) {
		this.orderService = orderService;
	}
	public List getOrderList() {
		return orderList;
	}
	public void setOrderList(List orderList) {
		this.orderList = orderList;
	}
	public Timestamp getStart() {
		return start;
	}
	public void setStart(Timestamp start) {
		this.start = start;
	}
	public Timestamp getEnd() {
		return end;
	}
	public void setEnd(Timestamp end) {
		this.end = end;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
