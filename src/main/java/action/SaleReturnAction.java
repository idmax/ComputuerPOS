package action;
import vo.Salereturn;
import vo.Orderitem;
import vo.Salereturnitem;
import service.ISaleReturnService;
import service.ISaleReturnItemService;
import com.opensymphony.xwork2.ActionSupport;
public class SaleReturnAction extends ActionSupport{
	private ISaleReturnService saleReturnService;
	private ISaleReturnItemService saleReturnItemService;
	private Salereturn saleReturn;
	private Salereturnitem saleReturnItem;
	private Orderitem orderItem;
	public String createSaleReturn(){
		saleReturnService.createSaleReturn(saleReturn);
		return SUCCESS;
	}
	public String addSaleReturnItem(){
		saleReturnItemService.addSaleReturnItem(saleReturnItem,orderItem.getOrderItemId());
		return SUCCESS;
	}
	
	
	
	
	public ISaleReturnService getSaleReturnService() {
		return saleReturnService;
	}
	public void setSaleReturnService(ISaleReturnService saleReturnService) {
		this.saleReturnService = saleReturnService;
	}
	public Salereturn getSaleReturn() {
		return saleReturn;
	}
	public void setSaleReturn(Salereturn saleReturn) {
		this.saleReturn = saleReturn;
	}
	public ISaleReturnItemService getSaleReturnItemService() {
		return saleReturnItemService;
	}
	public void setSaleReturnItemService(
			ISaleReturnItemService saleReturnItemService) {
		this.saleReturnItemService = saleReturnItemService;
	}
	public Salereturnitem getSaleReturnItem() {
		return saleReturnItem;
	}
	public void setSaleReturnItem(Salereturnitem saleReturnItem) {
		this.saleReturnItem = saleReturnItem;
	}
	public Orderitem getOrderItem() {
		return orderItem;
	}
	public void setOrderItem(Orderitem orderItem) {
		this.orderItem = orderItem;
	}
	
}
