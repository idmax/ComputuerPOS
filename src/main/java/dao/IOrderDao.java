package dao;
import vo.Order;
import java.sql.Timestamp;
import java.util.List;
public interface IOrderDao {
	public Order addOrder(Order o);
	public Order getOrderById(int id);
	public void setOrder(Order o);
	public List getOrderByNumber(String orderNumber);
	public List getAllOrder();
	public List getOrderByTime(Timestamp start,Timestamp end);
	public List getOrderByUserName(String userName);
}
