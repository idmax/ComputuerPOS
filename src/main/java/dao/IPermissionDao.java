package dao;
import vo.Permission;
import java.util.List;
public interface IPermissionDao {
	public void addPer(Permission per);
	public void deletePer(int id);
	public List getPer(int id);
	public List getAllPer();
	public void update(Permission per);
}
