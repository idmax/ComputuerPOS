package dao;

import vo.UserRole;
import java.util.List;

public interface IUserRoleDao {

		public void addUserRole(UserRole userRole);
		public void deleteUserRole(int id);
		public List getUserRoleByUserId(int userId);

}
