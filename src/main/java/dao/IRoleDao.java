package dao;
import vo.Role;
import java.util.List;
public interface IRoleDao {
	public List getRoleById(int id);
	public void addRole(Role role);
	public void deleteRole(int id);
	public List getAllRole();
	public void update(Role role);
}
