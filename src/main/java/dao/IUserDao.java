package dao;
import vo.Users;
import java.util.List;
public interface IUserDao {
public Users addUser(Users user);
public void deleteUser(String userNumber);
public List getUserByName(String name);
public void UpdateUserPasswd(int id,String passwd);
public List getAllUsers();
public List getUserById(int id);
public List getUserByUserNumber(String userNumber);
public void update(Users user);
}
