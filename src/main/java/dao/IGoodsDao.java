package dao;
import vo.Goods;
import java.util.List;
public interface IGoodsDao {
public void addGoods(Goods goods);
public List allGoods();
public void deleteGoods(int goodsNumber);
public void updateGoodsQty(int id,int qty);
public Goods getGoodsById(int id);
public List getGoodsByGoodsNumber(int goodsNumebr);
public void update(Goods goods);
//public List getGoodsByMultiCondition(Goods goods);
public List getGoodsByGoodsName(String goodsName);
}
