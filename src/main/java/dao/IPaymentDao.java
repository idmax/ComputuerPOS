package dao;
import vo.Payment;
public interface IPaymentDao {
	public void addPayment(Payment payment);
	public Payment getPaymentByNo(String paymentNo);
}
