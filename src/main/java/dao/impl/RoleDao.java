package dao.impl;

import vo.Role;
import vo.Type;
import dao.BaseDao;
import dao.IRoleDao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.Query;

import java.util.List;
public class RoleDao extends BaseDao implements IRoleDao {

	@Override
	public List getRoleById(int id) {
		Session ss=getSession();
		String hql="from Role where roleId=:id";
		Query q=ss.createQuery(hql);
		q.setParameter("id", id);
		List role=q.list();
		ss.close();
		return role;
	}
	public void addRole(Role role){
		Session ss=getSession();
		Transaction ts=ss.beginTransaction();
		ss.save(role);
		ts.commit();
		ss.close();
	}
	public List getAllRole(){
		Session ss=getSession();
		String hql="from Role";
		Query q=ss.createQuery(hql);
		List roles=q.list();
		ss.close();
		return roles;
	}
	public void deleteRole(int id) {
		Session session=getSession();
		String hql="delete Role where id=:id";
		Query query=session.createQuery(hql);
		query.setParameter("id",id);
		query.executeUpdate();
		session.close();

	}
	public void update(Role role){
		Session session=getSession();
		Transaction ts=session.beginTransaction();
		Role r=(Role)session.get(Role.class, role.getRoleId());
		if(role.getDespription()!=null){
			r.setDespription(role.getDespription());
		}
		if(role.getRoleName()!=null){
			r.setRoleName(role.getRoleName());
		}
		session.update(r);
		ts.commit();
		session.close();
	}
}
