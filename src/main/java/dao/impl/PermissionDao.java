package dao.impl;

import vo.Goods;
import vo.Permission;
import dao.BaseDao;
import dao.IPermissionDao;

import org.hibernate.Session;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.Query;

import java.util.List;
public class PermissionDao extends BaseDao implements IPermissionDao {

	@Override
	public void addPer(Permission per) {
		Session ss=getSession();
		Transaction ts=ss.beginTransaction();
		ss.save(per);
		ts.commit();
		ss.close();
	}

	@Override
	public void deletePer(int id) {
		Session session=getSession();
		String hql="delete Permission where id=:id";
		Query query=session.createQuery(hql);
		query.setParameter("id",id);
		query.executeUpdate();
		session.close();

	}
	public List getAllPer(){
		Session session=getSession();
		String hql="from Permission";
		Query query=session.createQuery(hql);
		List pers=query.list();
		session.close();
		return pers;
	}
	@Override
	public List getPer(int id) {
		Session session=getSession();
		String hql="from Permission where id=:id";
		Query query=session.createQuery(hql);
		query.setParameter("id", id);
		List per=query.list();
		session.close();
		return per;

	}
	public void update(Permission per){
		Session ss=getSession();
		Transaction ts=ss.beginTransaction();
		Permission p=(Permission)ss.get(Permission.class, per.getId());
		if(per.getDescription()!=null){
			p.setDescription(per.getDescription());
		}
		if(per.getPermissionName()!=null){
			p.setPermissionName(per.getPermissionName());
		}
		ss.update(p);
		ts.commit();
		ss.close();
	}

}
