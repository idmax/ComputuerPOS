package dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import vo.RolePermission;
import dao.BaseDao;
import dao.IRolePermissionDao;

public class RolePermissionDao extends BaseDao implements IRolePermissionDao {

	@Override
	public void add(RolePermission rp) {
		Session ss=getSession();
		Transaction ts=ss.beginTransaction();
		ss.save(rp);
		ts.commit();
		ss.close();

	}

	@Override
	public void delete(int id) {
		Session session=getSession();
		String hql="delete RolePermission where id=:id";
		Query query=session.createQuery(hql);
		query.setParameter("id",id);
		query.executeUpdate();
		session.close();

	}

	@Override
	public List getByRoleId(int roleId) {
		Session ss=getSession();
		String hql="from RolePermission where roleId=:id";
		Query q=ss.createQuery(hql);
		q.setParameter("id", roleId);
		List pers=q.list();
		ss.close();
		return pers;
	}

}
