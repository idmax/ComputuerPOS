package dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import vo.UserRole;
import dao.BaseDao;
import dao.IUserRoleDao;

public class UserRoleDao extends BaseDao implements IUserRoleDao {

	@Override
	public void addUserRole(UserRole userRole) {
		Session ss=getSession();
		Transaction ts=ss.beginTransaction();
		ss.save(userRole);
		ts.commit();
		ss.close();

	}

	@Override
	public void deleteUserRole(int id) {
		Session session=getSession();
		String hql="delete UserRole where id=:id";
		Query query=session.createQuery(hql);
		query.setParameter("id",id);
		query.executeUpdate();
		session.close();

	}

	@Override
	public List getUserRoleByUserId(int userId) {
		Session ss=getSession();
		String hql="from UserRole where userId=:id";
		Query q=ss.createQuery(hql);
		q.setParameter("id", userId);
		List role=q.list();
		ss.close();
		return role;
	}

}
