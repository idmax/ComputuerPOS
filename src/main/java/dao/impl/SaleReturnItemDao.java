package dao.impl;

import vo.Salereturnitem;
import dao.BaseDao;
import dao.ISaleReturnItemDao;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.Query;
import java.util.List;
import java.util.Iterator;
public class SaleReturnItemDao extends BaseDao implements ISaleReturnItemDao {

	@Override
	public void addSaleReturnItem(Salereturnitem saleReturnItem) {
		Session ss=getSession();
		Transaction ts=ss.beginTransaction();
		ss.save(saleReturnItem);
		ts.commit();
		ss.close();

	}

}
