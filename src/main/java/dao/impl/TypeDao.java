package dao.impl;

import java.util.List;

import vo.Goods;
import vo.Type;
import dao.BaseDao;
import dao.ITypeDao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.Query;
public class TypeDao extends BaseDao implements ITypeDao {

	@Override
	public void addType(Type type) {
		Session session=getSession();
		Transaction ts=session.beginTransaction();
		session.save(type);
		ts.commit();
		session.close();

	}

	@Override
	public List getAllType() {
		Session session=getSession();
		String hql="from Type";
		Query query=session.createQuery(hql);
		List types=query.list();
		session.close();
		return types;
	}

	@Override
	public void deleteType(int id) {
		Session session=getSession();
		String hql="delete Type where TypeID=?";
		Query query=session.createQuery(hql);
		query.setParameter(0, id);
		query.executeUpdate();
		session.close();
	}

	@Override
	public void updateTypename(int id,String name) {
		Session session=getSession();
		String hql="update Type set TypeName=? where TypeID=?";
		Query query=session.createQuery(hql);
		query.setParameter(0, name);
		query.setParameter(1, id);
		query.executeUpdate();
		session.close();

	}
	public List getTypeById(int id)
	{
		Session session=getSession();
		String hql="from Type where TypeID=?";
		Query query=session.createQuery(hql);
		query.setParameter(0, id);
		List types=query.list();
		session.close();
		return types;
	}
	public void update(Type type){
		Session session=getSession();
		Transaction ts=session.beginTransaction();
		Type t=(Type)session.get(Type.class, type.getTypeId());
		if(type.getDesription()!=null){
			t.setDesription(type.getDesription());
		}
		if(type.getTypeName()!=null){
			t.setTypeName(type.getTypeName());
		}
		session.update(t);
		ts.commit();
		session.close();
	}
	public List getTypeByTypeName(String typeName){
		Session session=getSession();
		String hql="from Type t where t.typeName like :typeName";
		Query query=session.createQuery(hql);
		query.setParameter("typeName","%"+typeName+"%");
		List types=query.list();
		session.close();
		return types;
	}
}
