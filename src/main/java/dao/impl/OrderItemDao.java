package dao.impl;

import vo.Orderitem;

import java.sql.Timestamp;

import dao.IOrderItemDao;
import dao.BaseDao;

import org.hibernate.Session;
import org.hibernate.Query;
import org.hibernate.Transaction;

import java.util.List;
public class OrderItemDao extends BaseDao implements IOrderItemDao{

	@Override
	public Orderitem addOrderItem(Orderitem oi) {
		Session session=getSession();
		Transaction ts=session.beginTransaction();
		session.save(oi);
		ts.commit();
		session.close();
		return oi;
	}
	public void updateOrderItemState(int orderItemId){
		Session session=getSession();
		String hql="update Orderitem set state=0 where orderItemId=:id";
		Query query=session.createQuery(hql);
		query.setParameter("id",orderItemId);
		query.executeUpdate();
		session.close();
	}
	public List getAllOrderItem(){
		Session s=getSession();
		String hql="from Orderitem";
		Query q=s.createQuery(hql);
		List orderItems=q.list();
		s.close();
		return orderItems;
	}
	public List getOrderItemByOrderId(int orderId){
		Session s=getSession();
		String hql="from Orderitem where order.orderId=:orderId";
		Query q=s.createQuery(hql);
		q.setParameter("orderId", orderId);
		List orderItems=q.list();
		s.close();
		return orderItems;
	}
	public List getOrderItemByTime(Timestamp start,Timestamp end){
		Session s=getSession();
		String hql="select oi from Orderitem as oi,Order as o where o.orderTime>:start and o.orderTime<:end and o.orderId=oi.order.orderId ";
		Query q=s.createQuery(hql);
		q.setParameter("start",start);
		q.setParameter("end", end);
		List orderItems=q.list();
		s.close();
		return orderItems;
	}
}
