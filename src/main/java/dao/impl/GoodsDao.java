package dao.impl;

import dao.BaseDao;
import dao.IGoodsDao;
import vo.Goods;
import org.hibernate.Session;
import org.hibernate.Query;
import org.hibernate.Transaction;
import java.util.List;
public class GoodsDao extends BaseDao implements IGoodsDao{
public void addGoods(Goods goods){
	Session session=getSession();
	Transaction ts=session.beginTransaction();
	session.save(goods);
	ts.commit();
	session.close();
}
public List allGoods(){
	Session session=getSession();
	String hql="from Goods";
	Query query=session.createQuery(hql);
	List goods=query.list();
	return goods;
}
	public void deleteGoods(int goodsNumber){
		Session session=getSession();
		String hql="delete Goods where GoodsNumber=:number";
		Query query=session.createQuery(hql);
		query.setParameter("number",goodsNumber);
		query.executeUpdate();
		session.close();
	}
	public void updateGoodsQty(int id,int qty){
		Session session=getSession();
		String hql="update Goods set inventory=? where GoodsID=?";
		Query query=session.createQuery(hql);
		query.setParameter(0, qty);
		query.setParameter(1, id);
		query.executeUpdate();
		session.close();
	}
	public Goods getGoodsById(int id){
		Session session=getSession();
		String hql="from Goods where GoodsID=?";
		Query query=session.createQuery(hql);
		query.setParameter(0, id);
		List gd=query.list();
		session.close();
		return (Goods)gd.get(0);
	}
	public List getGoodsByGoodsNumber(int goodsNumber){
		Session session=getSession();
		String hql="from Goods where GoodsNumber=?";
		Query query=session.createQuery(hql);
		query.setParameter(0, goodsNumber);
		List gd=query.list();
		session.close();
		return gd;
	}
	public void update(Goods goods){
		Session session=getSession();
		Transaction ts=session.beginTransaction();
		Goods gs=(Goods)session.get(Goods.class,goods.getGoodsId());
		if(goods.getDiscount()!=null){
			gs.setDiscount(goods.getDiscount());
		}
		if(goods.getGoodsName()!=null){
			gs.setGoodsName(goods.getGoodsName());
		}
		if(goods.getGoodsNumber()!=null){
			gs.setGoodsNumber(goods.getGoodsNumber());
		}
		if(goods.getPrice()!=null){
			gs.setPrice(goods.getPrice());
		}
		if(goods.getType()!=null){
			gs.setType(goods.getType());
		}
		session.update(gs);
		ts.commit();
		session.close();
	}
//	public List getGoodsByMultiCondition(Goods goods){
//		Session session=getSession();
//		String hql="from Goods o where o.goodsId like :goodsId and o.type like :type and o.goodsNumber like :goodsNumber and o.goodsName like :goodsName and o.price like :price and o.discount like :discount";
//		Query query=session.createQuery(hql);
//		query.setParameter("discount", (goods.getDiscount()!=null)?("%"+goods.getDiscount()+"%"):"%");
//		query.setParameter("goodsNumber", (goods.getGoodsNumber()!=null)?("%"+goods.getGoodsNumber()+"%"):"%");
//		query.setParameter("goodsName", (goods.getGoodsName()!=null)?("%"+goods.getGoodsName()+"%"):"%");
//		query.setParameter("type", (goods.getType()!=null)?("%"+goods.getType()+"%"):"%");
//		query.setParameter("price", (goods.getPrice()!=null)?("%"+goods.getPrice()+"%"):"%");
//		query.setParameter("goodsId", (goods.getGoodsId()!=null)?("%"+goods.getGoodsId()+"%"):"%");
//		List gd=query.list();
//		session.close();
//		return gd;
//	}
	public List getGoodsByGoodsName(String goodsName){
		Session session=getSession();
		String hql="from Goods g where g.goodsName like :goodsName";
		Query query=session.createQuery(hql);
		query.setParameter("goodsName","%"+goodsName+"%");
		List gd=query.list();
		session.close();
		return gd;
	}
}
