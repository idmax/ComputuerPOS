package dao.impl;

import vo.Payment;
import dao.BaseDao;
import dao.IPaymentDao;
import org.hibernate.Session;
import org.hibernate.Query;
import org.hibernate.Transaction;
import java.util.List;
public class PaymentDao extends BaseDao implements IPaymentDao {

	@Override
	public void addPayment(Payment payment) {
		Session ss=getSession();
		Transaction ts=ss.beginTransaction();
		ss.save(payment);
		ts.commit();
		ss.close();
	}

	@Override
	public Payment getPaymentByNo(String paymentNo) {
		Session ss=getSession();
		String hql="from Payment where paymentNo=:no";
		Query query=ss.createQuery(hql);
		query.setParameter("no",paymentNo);
		List payment=query.list();
		ss.close();
		return (Payment) payment.get(0);

	}

}
