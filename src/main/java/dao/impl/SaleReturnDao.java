package dao.impl;

import vo.Salereturn;
import dao.BaseDao;
import dao.ISaleReturnDao;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.Query;
import java.util.List;
import java.util.Iterator;
public class SaleReturnDao extends BaseDao implements ISaleReturnDao {

	@Override
	public void addSaleReturn(Salereturn sr) {
		Session ss=getSession();
		Transaction ts=ss.beginTransaction();
		ss.save(sr);
		ts.commit();
		ss.close();
	}

}
