package dao;
import vo.Type;
import java.util.List;
public interface ITypeDao {
	public void addType(Type type);
	public List getAllType();
	public void deleteType(int id);
	public void updateTypename(int id,String name);
	public List getTypeById(int id);
	public void update(Type type);
	public List getTypeByTypeName(String typeName);
}
