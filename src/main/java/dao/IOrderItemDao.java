package dao;
import vo.Orderitem;
import java.sql.Timestamp;
import java.util.List;
public interface IOrderItemDao {
	public Orderitem addOrderItem(Orderitem oi);
	public void updateOrderItemState(int orderItemId);
	public List getAllOrderItem();
	public List getOrderItemByOrderId(int orderId);
	public List getOrderItemByTime(Timestamp start,Timestamp end);
}
