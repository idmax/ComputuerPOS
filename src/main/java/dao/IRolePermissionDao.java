package dao;
import vo.RolePermission;
import java.util.List;
public interface IRolePermissionDao {
	public void add(RolePermission rp);
	public void delete(int id);
	public List getByRoleId(int roleId);
}
