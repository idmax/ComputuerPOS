package four;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import service.ISaleReturnService;
import vo.Salereturn;
import vo.Order;
import service.IOrderService;
public class testSaleReturn {
	private ApplicationContext ac;
	private ISaleReturnService srs;
	private IOrderService orderService;
	public testSaleReturn(){
		ac=new ClassPathXmlApplicationContext("ApplicationContext.xml");
		srs=(ISaleReturnService)ac.getBean("saleReturnService");
		orderService=(IOrderService)ac.getBean("orderService");
	}
	public static void main(String[] args){
		testSaleReturn tsr=new testSaleReturn();
		Salereturn saleReturn=new Salereturn();
		Order o=tsr.orderService.getOrderByNumber("number");
		saleReturn.setOrder(o);
		saleReturn.setReason("the reason");
		saleReturn.setSalereturnNo("NO");
		saleReturn.setTotal(12.2);
		tsr.srs.createSaleReturn(saleReturn);
	}
}
