package first;
import vo.Goods;
import java.util.List;
import dao.IGoodsDao;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
public class testGoods {
	ApplicationContext ac;
	IGoodsDao goodsdao;
	public testGoods(){
		ac=new ClassPathXmlApplicationContext("ApplicationContext.xml");
		goodsdao=(IGoodsDao)ac.getBean("goodsDao");
		
	}
	public void listGoods(){
		List goods=this.goodsdao.allGoods();
		if(goods.size()!=0){
			System.out.println("Goods表的所有商品：");
			Goods gd;
			for(int i=0;i<goods.size();i++){
				gd=(Goods)goods.get(i);
				System.out.println("ID:"+gd.getGoodsId()+",商品名："+gd.getGoodsName());
				
			}
		}
	}
	public static void main(String[] args) {
		testGoods tg=new testGoods();
		//添加商品
		System.out.println("添加商品巧克力：");
		Goods gd=new Goods("巧克力",12,1,32);
		System.out.println("未添加商品之前：");
		tg.listGoods();
		tg.goodsdao.addGoods(gd);
		System.out.println("添加商品之后：");
		tg.listGoods();
		//删除商品
		System.out.println("删除商品：");
		tg.goodsdao.deleteGoods(2);
		System.out.println("删除商品ID为2的商品之后：");
		tg.listGoods();
		//查询并修改商品库存数
		Goods goods=tg.goodsdao.getGoodsById(2);
		System.out.println("查询商品ID为2的商品库存："+goods.getInventory());
		System.out.println("修改库存数为100");
		tg.goodsdao.updateGoodsQty(2,100);
		goods=tg.goodsdao.getGoodsById(2);
		System.out.println("修改后商品库存为："+goods.getInventory());
	}

}
