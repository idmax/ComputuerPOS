package first;
import java.util.List;

import dao.ITypeDao;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import vo.Type;
public class testType {
	ApplicationContext ac;
	ITypeDao typedao;
	public testType(){
		ac=new ClassPathXmlApplicationContext("ApplicationContext.xml");
		typedao=(ITypeDao)ac.getBean("typeDao");
		
	}
	public void listtype(){
		List type=this.typedao.getAllType();
		if(type.size()!=0){
			System.out.println("type表的所有类型：");
			Type tp;
			for(int i=0;i<type.size();i++){
				tp=(Type)type.get(i);
				System.out.println("ID:"+tp.getTypeId()+",商品名："+tp.getTypeName());
				
			}
		}
	}
	public static void main(String[] args) {
		testType tt=new testType();
		//添加商品类型
		System.out.println("未添加类型之前：");
		tt.listtype();
		Type type=new Type("厨房用具","相关描述");
		tt.typedao.addType(type);
		System.out.println("添加之后：");
		tt.listtype();
		//删除商品类型
		System.out.println("删除商品类型：");
		tt.listtype();
		System.out.println("删除ID为7的商品类型之后：");
		tt.typedao.deleteType(7);
		tt.listtype();
		//查询并修改商品类型名称
		System.out.println("查询并修改商品类型名称：");
		System.out.println("修改之前：");
		tt.listtype();
		tt.typedao.updateTypename(2, "糖果");
		System.out.println("商品类型Id为2的名称为糖果之后：");
		tt.listtype();
		
	}

}
