package first;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import java.util.List;
import vo.Users;
import dao.IUserDao;
import dao.impl.UserDao;
public class testUser {
	ApplicationContext ctx;
	IUserDao userdao;
	public testUser(){
		ctx=new ClassPathXmlApplicationContext("applicationContext.xml");
		userdao=(UserDao)ctx.getBean("userDao");
	}
public void listUsers(){
	List users=this.userdao.allUsers();
	if(users.size()!=0){
		System.out.println("User表的所有用户：");
		Users user;
		for(int i=0;i<users.size();i++){
			user=(Users)users.get(i);
			System.out.println(user.getUserName());
			
		}
	}
}
public Users getUserByName(String name){
	List us=this.userdao.selectUserByName(name);
		Users u=(Users)us.get(0);
		return u;
	
}
	public static void main(String[] args) {
		
		testUser ts=new testUser();
		Users user=new Users();
		//添加用户
		System.out.println("添加用户sun：");
		user.setUserName("sun");
		user.setUserPasswd("123");
		user.setGender("w");
		user.setTel(123);
		System.out.println("未添加用户之前");
		ts.listUsers();
		System.out.println("添加用户之后");
		ts.userdao.addUser(user);
		ts.listUsers();
		////查询用户、删除用户
		System.out.println();
		System.out.println("查询用户、删除用户:");
		System.out.println("现在查找用户名为yajin的记录");
		Users yajin=ts.getUserByName("yajin");
			System.out.println("yajin 的id为"+yajin.getUserId());
			System.out.println("现在删除这条记录,删除之后：");
			ts.userdao.deleteUser(yajin.getUserId());
			ts.listUsers();
			
		//修改用户密码
		System.out.println();
		System.out.println();
		System.out.println("修改用户密码:");
		Users haha=ts.getUserByName("haha");
			System.out.println("haha 的密码为"+haha.getUserPasswd());
			System.out.println("现在修改为000，修改之后：");
			ts.userdao.UpdateUserPasswd(haha.getUserId(), "000");
			 haha=ts.getUserByName("haha");
			System.out.println("haha 的密码为"+haha.getUserPasswd());
		
		
	}

}
